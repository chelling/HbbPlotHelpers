#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <getopt.h>
#include <iomanip>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "EventWideHbbAnalysis.h"

// -- Options --
uint32_t nthreads=0;
std::string reweightMapName;
std::string ewCorrections;
//

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] outDir [sample]" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -t, --threads  Number of threads (default: " << nthreads << ")" << std::endl;
  std::cerr << " -r, --reweight Path to file containing (pT/m) reweight maps (default: none)" << std::endl;
  std::cerr << " -e, --ewcorr Path to file containing EW corrections (default: none)" << std::endl;
  std::cerr << "" << std::endl;
}

struct CutflowStep
{
  std::string name;
  std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::* node;
  std::string weight_column;
};

int main(int argc, char* argv[])
{
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
        {
          {"threads" ,  required_argument, 0,  't' },
          {"reweight",  required_argument, 0,  'r' },
	  {"ewcorr"  ,  required_argument, 0,  'e' },
          {         0,                  0, 0,   0  }
        };

      c = getopt_long(argc, argv, "t:r:e:", long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 't':
          nthreads = std::stoi(optarg);
          break;
        case 'r':
          reweightMapName=optarg;
          break;
        case 'e':
	  ewCorrections=optarg;
          break;
        default:
          std::cerr << "Invalid option supplied. Aborting." << std::endl;
          std::cerr << std::endl;
          usage(argv);
	  return 1;
        }
    }

  if (optind == argc)
    {
      std::cerr << "Required outDir argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  // Parse arguments
  std::string outDir=argv[optind++];
  std::vector<std::string> saves={"Higgs_ggf","Higgs_VBF","Higgs_VH","Higgs_ttH","data","Pythia8_dijet","Higgs","Powheg_ttbar","Sherpa_Zqq","Sherpa_Wqq"};

  if(optind<argc)
    saves={argv[optind++]};
  
  ROOT::EnableImplicitMT(nthreads);  

  //
  // Define steps in the cutflow
  std::vector<CutflowStep> steps;
  steps.push_back({"Trigger"     ,&EventWideHbbAnalysis::df_trigger      , "weight_trigger"});
  steps.push_back({"TriggerJet"  ,&EventWideHbbAnalysis::df_triggerjet   , "weight_trigger"});
  steps.push_back({"JetCleaning" ,&EventWideHbbAnalysis::df_cleaning     , "weight_trigger"});
  steps.push_back({"Dijet"       ,&EventWideHbbAnalysis::df_dijet        , "weight_trigger"});
  steps.push_back({"JetList"     ,&EventWideHbbAnalysis::df_fatjet_inlist, "weight_trigger"});
  steps.push_back({"SRL"         ,&EventWideHbbAnalysis::df_srl_inc      , "w"             });
  steps.push_back({"SRS"         ,&EventWideHbbAnalysis::df_srs_inc      , "w"             });
  steps.push_back({"VRL"         ,&EventWideHbbAnalysis::df_vrl_inc      , "w"             });
  steps.push_back({"VRS"         ,&EventWideHbbAnalysis::df_vrs_inc      , "w"             });

  //
  // Loop over samples
  SampleFactory sf("../samples.yml");

  for(const std::string& save : saves)
    {
      std::string outFile=outDir+"/cutflow_"+save+".root";
      TFile *fh_out=TFile::Open(outFile.c_str(),"RECREATE");

      std::cout << "Processing " << save << std::endl;
      TH1F *h_cutflow=new TH1F(("cutflow_"+save).c_str(),"",steps.size(),-0.5,steps.size()-0.5);      
      if(!sf.contains(save))
	{
	  std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
	  continue;
	}
      EventWideHbbAnalysis a(sf[save]->dataframe(), sf[save]->isMC(), "", "Higgs", "nHBosons", "MV2c10_FixedCutBEff_77", sf[save]->sumW(), reweightMapName, ewCorrections);

      uint32_t binidx=1;
      for(const CutflowStep& step : steps)
	{
	  std::cout << " " << step.name << std::endl;
	  h_cutflow->GetXaxis()->SetBinLabel(binidx, step.name.c_str());
	  double nEvents=(a.*(step.node))->Sum(step.weight_column).GetValue();
	  h_cutflow->SetBinContent(binidx, nEvents);
	  ++binidx;
	}
      fh_out->cd();
      h_cutflow->Write();
      fh_out->Close();
    }

  return 0;
}
