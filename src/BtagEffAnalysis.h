#ifndef BTAGEFFANALYSIS_H
#define BTAGEFFANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class BtagEffAnalysis
{
public:
  BtagEffAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, const std::string& btagWP="MV2c10_FixedCutBEff_77");

  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;

  std::shared_ptr<ROOT::RDF::RNode> df_fatjet;
  std::shared_ptr<ROOT::RDF::RNode> df_trkjet;
  std::shared_ptr<ROOT::RDF::RNode> df_def;
};

#endif // BTAGEFFANALYSIS_H
