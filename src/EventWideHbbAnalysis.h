#ifndef EVENTWIDEHBBANALYSIS_H
#define EVENTWIDEHBBANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>
#include <TH2.h>
#include <TF1.h>
#include <TFile.h>

#include "WeightDB.h"
#include "EWReweightTool.h"

class EventWideHbbAnalysis
{
public:
  /**
   * Construct the selection for the boosted Hbb+jet signal and validation regions
   *
   * \param rootdf base RDF node on which to start the selection
   * \param isMC indicate whether running over MC (true) or data (false)
   * \param syst suffix of a fat jet systematic, leave blank for nominal
   * \param btagWP b-tagging working point to use
   * \param br_truth input branch prefix to save into the output "truth_*" columns
   * \param br_truthTag input branch without the `fatjet_` suffix to save into the output of `Hcand_nTruthBosons`
   * \param SumW The key is the sample DSID, value is a list with each element being the corresponding weight's sumW.
   * \param reweightMapName Name of the file containing the reweighting map
   * \param ewCorrections Name of the file containing the EW corrections for Higgs
   */
  EventWideHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& syst="", const std::string& br_trutht="Higgs", const std::string& br_truthTag="nHBosons", const std::string& btagWP="MV2c10_FixedCutBEff_77", const std::unordered_map<uint32_t, std::vector<double>>& SumW={}, const std::string& reweightMapName = "", const std::string& ewCorrections = "");
  ~EventWideHbbAnalysis();

  std::vector<std::string> extra_weights={"MUR0p5_MUF0p5","MUR0p5_MUF1p0","MUR1p0_MUF0p5","MUR1p0_MUF1p0","MUR1p0_MUF2p0","MUR2p0_MUF1p0","MUR2p0_MUF2p0","Var3cUp","Var3cDown","isr_muRfac1p0_fsr_muRfac2p0","isr_muRfac1p0_fsr_muRfac0p5"};
  
  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;
  std::shared_ptr<ROOT::RDF::RNode> df_deftr;

  std::shared_ptr<ROOT::RDF::RNode> df_prw;
  std::shared_ptr<ROOT::RDF::RNode> df_trigger;
  std::shared_ptr<ROOT::RDF::RNode> df_triggerjet;
  std::shared_ptr<ROOT::RDF::RNode> df_cleaning;
  std::shared_ptr<ROOT::RDF::RNode> df_dijet;

  std::shared_ptr<ROOT::RDF::RNode> df_trkJet5Idx;
  std::shared_ptr<ROOT::RDF::RNode> df_trkJet10Idx;

  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_ntj;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_nbin2;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_vrolap;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_boost;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_inlist;

  std::shared_ptr<ROOT::RDF::RNode> df_truth;
  std::shared_ptr<ROOT::RDF::RNode> df_category;

  // Signal Region Leading
  std::shared_ptr<ROOT::RDF::RNode> df_srl_inc;

  // Signal Region Subleading
  std::shared_ptr<ROOT::RDF::RNode> df_srs_inc;

  // Validation Region Leading
  std::shared_ptr<ROOT::RDF::RNode> df_vrl_inc;

  // Validation Region Subleading
  std::shared_ptr<ROOT::RDF::RNode> df_vrs_inc;


private:
  std::shared_ptr<ROOT::RDF::RNode> Define_Hcand  (std::shared_ptr<ROOT::RDF::RNode> df, uint32_t Hcand_idx, const std::string& syst);
  std::shared_ptr<ROOT::RDF::RNode> Define_Weights(std::shared_ptr<ROOT::RDF::RNode> df);

  //
  // Useful tools
  WeightDB m_weightdb;
  EWReweightTool m_ewCorrections;

  //
  // Reweighting support

  TFile* m_fMaps=nullptr;

  std::map<std::string,TH2*> m_sysMaps;
  
  void LoadReweightMaps();

  //
  // Settings used to define the selection

  // Simulated events
  bool m_isMC=false;

  // Fat jet systematics variation
  std::string m_syst="";

  // Name of the truth particle branch
  std::string m_br_truth="Higgs";

  // Name of the jet truth tag particle branch (suffix to fatjet_)
  std::string m_br_truthTag="nHBosons";

  // B-tagging working point
  std::string m_btagWP="MV2c10_FixedCutBEff_77";

  // SumW for different alternate MC weights
  std::unordered_map<uint32_t, std::vector<double>> m_SumW;

  //! Path to file with 2D reweighting maps
  std::string m_reweightMapName = "";
};

#endif // EVENTWIDEHBBANALYSIS_H
