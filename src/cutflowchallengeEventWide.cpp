#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "EventWideHbbAnalysis.h"

int main()
{
  ROOT::EnableImplicitMT();

  SampleFactory sf("../samples.yml");

  EventWideHbbAnalysis a(sf["Higgs_ggf.mc16d"]->dataframe(), sf["Higgs_ggf.mc16d"]->isMC());

  std::cout << "Preselected\t"    << a.df_root         ->Count().GetValue() << "\t" << a.df_root         ->Sum("weight_norm"   ).GetValue()*44.3e3 << std::endl;
  std::cout << "Trigger\t"        << a.df_trigger      ->Count().GetValue() << "\t" << a.df_trigger      ->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Trigger Jet\t"    << a.df_triggerjet   ->Count().GetValue() << "\t" << a.df_triggerjet   ->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Jet Cleaning\t"   << a.df_cleaning     ->Count().GetValue() << "\t" << a.df_cleaning     ->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Dijet\t"          << a.df_dijet        ->Count().GetValue() << "\t" << a.df_dijet        ->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Jet List\t"       << a.df_fatjet_inlist->Count().GetValue() << "\t" << a.df_fatjet_inlist->Sum("weight_trigger").GetValue() << std::endl;

  std::cout << std::endl;
  std::cout << "SRL\t"          << a.df_srl_inc        ->Count().GetValue() << "\t" << a.df_srl_inc      ->Sum("w"             ).GetValue() << std::endl;
  std::cout << "SRS\t"          << a.df_srs_inc        ->Count().GetValue() << "\t" << a.df_srs_inc      ->Sum("w"             ).GetValue() << std::endl;
  std::cout << "VRL\t"          << a.df_vrl_inc        ->Count().GetValue() << "\t" << a.df_vrl_inc      ->Sum("w"             ).GetValue() << std::endl;
  std::cout << "VRS\t"          << a.df_vrs_inc        ->Count().GetValue() << "\t" << a.df_vrs_inc      ->Sum("w"             ).GetValue() << std::endl;

  return 0;
}
