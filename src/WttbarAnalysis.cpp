#include "WttbarAnalysis.h"

#include "Sample.h"
#include "Defines.h"
#include "WttbarDefines.h"

WttbarAnalysis::WttbarAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst,
															 const std::string& br_truth_pt, const std::string& btagWP, 
															 const std::unordered_map<uint32_t, std::vector<double>>& SumW,
															 const std::string& reweightMapName, const std::string& ewCorrections)
                               : m_isMC(isMC), m_SumW(SumW), m_reweightMapName(reweightMapName),
															   m_ewCorrections(ewCorrections)
{

  // Load information
  // V+jets
  m_weightdb.scanFile(700040, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700041, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700042, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700043, "../data/weightdb/Sh228.txt");
	m_weightdb.scanFile(700044, "../data/weightdb/Sh228.txt");
	m_weightdb.scanFile(700045, "../data/weightdb/Sh228.txt");
  
	// ttbar
  m_weightdb.scanFile(410470, "../data/weightdb/PowPy8_ttbar.txt");
  m_weightdb.scanFile(410471, "../data/weightdb/PowPy8_ttbar.txt");

	// Load reweighted maps
	LoadReweightMaps();

  // Create the RDF graph

  df_root=rootdf;
  // Filter out events without at least one muon and a fatjet w/ pt > 200.0
  df_filtered = std::make_shared<ROOT::RDF::RNode> ((*df_root)
						    .Filter([](int32_t nmuon)->bool 
									      {return nmuon > 0;}, {"nmuon"})
								.Filter([](const std::vector<float>& muon_pt)->bool
									      {return muon_pt[0] > 27.0;}, {"muon_pt"})
								.Filter([](const std::vector<float>& muon_eta)->bool
											  {return muon_eta[0] < 2.5;}, {"muon_eta"})
						    .Filter([](int32_t nfatjet)->bool
								 	      {return nfatjet > 0;}, {"nfatjet" + syst})
						    .Filter([](const std::vector<float>& fatjet_pt)->bool
							          {return fatjet_pt[0] > 200.0;}, {"fatjet" + syst + "_pt"})
								);

  // Definitions of four-momenta
  df_defp4 = std::make_shared<ROOT::RDF::RNode> ((*df_filtered)
						 .Define("fatjet" + syst + "_p4", PtEtaPhiM, 
						 {"nfatjet" + syst, "fatjet" + syst + "_pt", 
						  "fatjet" + syst + "_eta", "fatjet" + syst + "_phi", 
						  "fatjet" + syst + "_m"})
						 .Define("trkjet_p4", PtEtaPhiE,
						 {"ntrkjet", "trkjet_pt", "trkjet_eta", "trkjet_phi", "trkjet_E"})
						 .Define("muon_p4", PtEtaPhiM,
						 {"nmuon", "muon_pt", "muon_eta", "muon_phi", "muon_m"})
						 );

  df_prw = std::make_shared<ROOT::RDF::RNode> ((*df_defp4)
					 .Define("weight_prw",[](float weight, float weight_pileup)->float
					        { return weight*weight_pileup;}, {"weight_norm", "weight_pileup"}));

  if(isMC)
  {
		df_prw = std::make_shared<ROOT::RDF::RNode>((*df_prw)
				     .Define("truth_pt", [](float truth_pt)->float
						 {return truth_pt;}, {br_truth_pt})
						 .Define("ewcorr", [this](int mcChannelNumber, float truth_pt,
								     const std::vector<float>& mcEventWeights)->float {
						 switch(mcChannelNumber)
						 {
						   //Higgs corrections from a file
							 case 345054:
							   return (this->m_EW_WpH)?(1+this->m_EW_WpH->Eval(truth_pt)):1.0;
							 case 345053:
							   return (this->m_EW_WmH)?(1+this->m_EW_WmH->Eval(truth_pt)):1.0;
							 case 346641:
							 case 346640:
							 case 346639:
							 	 return (this->m_EW_VH)?(1+this->m_EW_VH->Eval(truth_pt)):1.0;
							 case 345055:
							 	 return (this->m_EW_ZllH)?(1+this->m_EW_ZllH->Eval(truth_pt)):1.0;
							 case 345056:
							   return (this->m_EW_ZvvH)?(1+this->m_EW_ZvvH->Eval(truth_pt)):1.0;
							 case 345949:
							   return (this->m_EW_VBF)?(1+this->m_EW_VBF->Eval(truth_pt)):1.0;
							 case 346343:
							 case 346344:
							 case 346345:
							 	 return (this->m_EW_ttH)?(1+this->m_EW_ttH->Eval(truth_pt)):1.0;

							 // V+jets corrections from an alternate weight
							 case 700040:
							 case 700041:
							 case 700042:
							 case 700043:
							 case 700044:
							 case 700045:
							   return mcEventWeights[298]/mcEventWeights[0];

							 // Others
							 default:
						     return 1.0;
						 }}, {"mcChannelNumber", "truth_pt", "mcEventWeights"})
						 .Define("weight_ew", [](float weight, float correction)->float 
						        {return weight*correction;}, {"weight_prw", "ewcorr"})
					   );	 


    df_trigger = std::make_shared<ROOT::RDF::RNode> ((*df_prw)
						     .Filter(filter_trigger_or2, {"trigger_HLT_mu26_ivarmedium", "trigger_HLT_mu20_iloose_L1MU15"})
						     .Define("weight_trigger", [](int runNumber, float weight) -> float {
							     switch(runNumber)
							     { // Need to check these luminosities
							       case 284500: return 36.2e3*weight; //mc16a
							       case 300000: return 44.3e3*weight; //mc16d
							       case 310000: return 58.5e3*weight; //mc16e
							     }
							       return 1.;}, {"runNumber", "weight_prw"}));
	}
	else
    df_trigger = std::make_shared<ROOT::RDF::RNode> ((*df_prw)
						     .Filter(filter_trigger_or2, {"trigger_HLT_mu26_ivarmedium", "trigger_HLT_mu20_iloose_L1MU15"})
						     .Define("weight_trigger", []()-> float { return 1.;}));
  
  df_triggermuon = std::make_shared<ROOT::RDF::RNode> ((*df_trigger)
						       .Filter([](const std::vector<std::string>& muon_listTrigChains,
									 const std::vector<std::vector<int32_t>>& 
									 muon_isTrigMatchedToChain)->bool {
									 	bool matched = false;
							   	 for(uint32_t i=0;i<muon_listTrigChains.size();i++)
							     {if(muon_listTrigChains[i]=="HLT_mu26_ivarmedium") 
									 	if (muon_isTrigMatchedToChain[0][i]) matched = true;
									  else if (muon_listTrigChains[i]=="HLT_mu20_iloose_L1MU15") 
									 	if (muon_isTrigMatchedToChain[0][i]) matched = true;}
									 	return matched;}, 
									 {"muon_listTrigChains","muon_isTrigMatchedToChain"})
						       );
  
  // cleaning
  df_cleaning = std::make_shared<ROOT::RDF::RNode> ((*df_triggermuon)
						   .Filter(filter_flag, {"eventClean_LooseBad"}));
 
	df_selection = std::make_shared<ROOT::RDF::RNode> ((*df_cleaning)
								 .Define("trkjet_R", trackjet_VR, {"trkjet_pt"})
								 .Define("fatjet_trkjet10_idxs", fatjet_trkjet10_idxs,
								 {"fatjet" + syst + "_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_pt"})
                 .Filter([](const std::vector<std::vector<uint32_t>>& trkjet10_idxs)->bool
                 {return trkjet10_idxs[0].size() > 1;}, {"fatjet_trkjet10_idxs"})
								 .Define("muon_vrjet_idxs", Wttbar::muon_vrjet_idxs,
								 {"muon_p4", "trkjet_p4", "trkjet_R", "trkjet_is_MV2c10_FixedCutBEff_77"})
								 .Filter([](const std::vector<int> vrjet_idxs)->bool
								 {return vrjet_idxs.back() != -99;}, {"muon_vrjet_idxs"})
								 .Define("bjet_idx", [](const std::vector<int> vrjet_idxs)->int
								 {return vrjet_idxs.back();}, {"muon_vrjet_idxs"})
								 .Filter([](const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4,
								 const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4,
								 const int bjet_idx)-> bool
								 {return fatjet_p4[0].DeltaR(trkjet_p4[bjet_idx]) > 2.0;},
								 {"fatjet" + syst + "_p4", "trkjet_p4", "bjet_idx"})
								 .Define("W_vrjet_idxs", Wttbar::W_vrjet_idxs,
                 {"fatjet" + syst + "_p4", "trkjet_p4", "trkjet_is_MV2c10_FixedCutBEff_77"})
								 .Filter([](const std::vector<int> vrjet_idxs)-> bool
								 {return vrjet_idxs.back() != -99;}, {"W_vrjet_idxs"})	 
								 );

	df_wttbar = std::make_shared<ROOT::RDF::RNode> ((*df_selection));

  // Wttbar
  if (m_isMC)
    {
      // b-tagging systematics loop
			const std::vector<std::string> the_btagging_systematics = 
				    (syst.empty() && btagging_systematics.count(btagWP) != 0) ?
						btagging_systematics.at(btagWP) : std::vector<std::string>({""});
      
			for (uint32_t btag_syst_idx = 0; btag_syst_idx < the_btagging_systematics.size(); btag_syst_idx++)
	  	{
	  		std::string bsystname = the_btagging_systematics[btag_syst_idx];
	  		df_wttbar = std::make_shared<ROOT::RDF::RNode>((*df_wttbar)
										.Define("w" + bsystname, 
										[btag_syst_idx](const float weight, const std::vector<int>& W_vrjet_idxs,
										const std::vector<int>& muon_vrjet_idxs, 
										const std::vector<std::vector<float>>& trkjet_SF)->float 
										{return weight*Wttbar::btagSF(W_vrjet_idxs, muon_vrjet_idxs, 
												                          trkjet_SF, btag_syst_idx);}, 
										{"weight_trigger", "W_vrjet_idxs", "muon_vrjet_idxs", "trkjet_SF_MV2c10_FixedCutBEff_77"})
										);
			}

      df_wttbar = Define_Weights(df_wttbar);
    }
  else
    {
      df_wttbar = std::make_shared<ROOT::RDF::RNode> ((*df_wttbar)
						                                          .Define("w", []()->float { return 1.; })
																											);
    }

  // other useful defines
  df_wttbar = std::make_shared<ROOT::RDF::RNode> ((*df_wttbar)
						  .Define("Hcand_pt", [](const std::vector<float> fatjet_pt)->float 
							{return fatjet_pt[0];}, {"fatjet" + syst + "_muonCorrected_pt"})
						  .Define("Hcand_uncorr_pt", [](const std::vector<float> fatjet_pt)->float
							{return fatjet_pt[0];}, {"fatjet" + syst + "_pt"})
						  .Define("Hcand_m", [](const std::vector<float> fatjet_m)->float 
							{return fatjet_m[0];}, {"fatjet" + syst + "_muonCorrected_m"})
						  .Define("Hcand_uncorr_m", [](const std::vector<float> fatjet_m)->float
							{return fatjet_m[0];}, {"fatjet" + syst + "_m"})
						  );
    
  if (!m_reweightMapName.empty() && m_isMC)
  {
    df_wttbar = std::make_shared<ROOT::RDF::RNode> ((*df_wttbar)
                .Define("truthjet_p4", PtEtaPhiM,
                {"ntruthfatjet", "truthfatjet_pt", "truthfatjet_eta",
                "truthfatjet_phi", "truthfatjet_m"})
                .Define("Hcand_eta", define_float_idx,
                {"fatjet" + syst + "_muonCorrected_eta", "Hcand_idx"})
                .Define("Hcand_phi", define_float_idx,
                {"fatjet" + syst + "_muonCorrected_phi", "Hcand_idx"})
                .Define("Hcand_truth_idx", matchdR_idx,
                {"Hcand_pt", "Hcand_eta", "Hcand_phi", "Hcand_m", "truthjet_p4"})
                .Define("Hcand_truth_pt",
                [](const std::vector<float>& truthfatjet_pt, int32_t Hcand_truth_idx)->float
                {return truthfatjet_pt[Hcand_truth_idx];}, {"truthfatjet_pt", "Hcand_truth_idx"})
                .Define("Hcand_truth_m",
                [](const std::vector<float>& truthfatjet_m, int32_t Hcand_truth_idx)->float
                {return truthfatjet_m[Hcand_truth_idx];}, {"truthfatjet_m", "Hcand_truth_idx"})
                .Define("reweight",
                [this](const float Hcand_truth_m, const float Hcand_truth_pt)->float
                {return getReweight(this->m_sysMaps["wttbar"], Hcand_truth_m, Hcand_truth_pt);},
                {"Hcand_truth_m", "Hcand_truth_pt"})
								);
  }

}

WttbarAnalysis::~WttbarAnalysis()
{
	// Close configuration files
  if (m_fMaps)
  {
		m_fMaps->Close();
 		m_fMaps = nullptr;
  }

	if (m_fEW)
	{
		m_fEW->Close();
		m_fEW = nullptr;
	}
}

std::shared_ptr<ROOT::RDF::RNode> WttbarAnalysis::Define_Weights(std::shared_ptr<ROOT::RDF::RNode> df)
{
  for(const std::string& wname : extra_weights)
  {
  	const std::unordered_map<uint32_t, uint32_t>& systidxmap = m_weightdb.systIdx(wname);
		//std::cout << "w + name: " << "w" + wname << std::endl;
    df = std::make_shared<ROOT::RDF::RNode>((*df)
				.Define("w"+wname, 
				[this,systidxmap](int runNumber, int mcChannelNumber, float weight, 
				const std::vector<float>& mcEventWeights)->float {
				if(systidxmap.count(mcChannelNumber) == 1 && this->m_SumW.count(dskey(runNumber,mcChannelNumber)) == 1)
				{
				  uint32_t systidx = systidxmap.at(mcChannelNumber);
					const std::vector<double>& sumw = this->m_SumW.at(dskey(runNumber,mcChannelNumber));
					return weight*
								(mcEventWeights.at(systidx)/sumw.at(systidx))/
								(mcEventWeights.at(0)/sumw.at(0));
				}
				else
					return weight;},
				{"runNumber", "mcChannelNumber", "w", "mcEventWeights"})
				);
  }

	// Remove EW Corrections
	df = std::make_shared<ROOT::RDF::RNode>((*df)
			 .Define("wNOEW", [](float weight, float ewcorr)->float
			 {return weight/ewcorr;}, {"w", "ewcorr"})
			 );

  return df;
}

void WttbarAnalysis::LoadReweightMaps()
{
	// Load 2D maps
	if (!m_reweightMapName.empty())
	{
		m_fMaps = TFile::Open(m_reweightMapName.c_str());
		std::vector<std::string> regions = {"wttbar"};
		for (std::string reg : regions)
		{
			std::string mapName = "hMap_" + reg;
			m_sysMaps.insert(std::pair<std::string, TH2*>(reg, m_fMaps->Get<TH2>(mapName.data())));
		}
	}

	// Load EW corrections
	if (!m_ewCorrections.empty())
	{
		m_fEW=TFile::Open(m_ewCorrections.c_str());
    m_EW_WmH =m_fEW->Get<TF1>("WmH" );
    m_EW_WpH =m_fEW->Get<TF1>("WpH" );
    m_EW_ZllH=m_fEW->Get<TF1>("ZHll");
    m_EW_ZvvH=m_fEW->Get<TF1>("ZHvv");
    m_EW_VH  =m_fEW->Get<TF1>("VH"  );
    m_EW_VBF =m_fEW->Get<TF1>("VBF" );
    m_EW_ttH =m_fEW->Get<TF1>("ttH" );
	}
}
