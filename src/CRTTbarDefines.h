#ifndef CRTTBARDEFINES_H_
#define CRTTBARDEFINES_H_

#include <stdint.h>

#include <vector>

#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

namespace CRTTbar
{
  //! Tag jet idx (matched to the leading muon)
  /**
   * Tag fatjet that is the closest fatjet to tag muon given pT and dR requirements
   *
   * \return index of a tag fatjet, 666 if not found
   */
  uint32_t tag_fatjet_idx(const ROOT::VecOps::RVec<TLorentzVector>& muon_p4, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4);

  //! Probe fatjet idx (one of the leading jets that is not tag jet)
  /**
   * \return probe fatjet idx
   */
  uint32_t probe_fatjet_idx(uint32_t tag_fatjet_idx);

  // T/F if the tag and probe jets are back to back
  bool filter_probe_tag_deltaPhi(uint32_t tag_fatjet_idx, uint32_t probe_fatjet_idx, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4);

  //! List of VR jets associated to the tag jet
  /**
   * Includes
   *  - overlap removal with a muon based on dR/VR<0.2
   *  - 10 GeV cut on track jets
   *
   * \param tag_fatjet_idx index of fatjet in fatjet lists
   * \param fatjet_trkjet_idxs list of track jets associated to fat jets to be slimmed down
   * \param trkjet_pt list of track jet pT's
   * \param trkjet_R list of track jet radii
   * \param trkjet_muon_dR list of dR of track jets to the muon
   *
   * \return list of idx's of track jets associated to the tag jet
   */
  std::vector<uint32_t> tag_vrjets(uint32_t tag_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<float>& trkjet_pt, const ROOT::VecOps::RVec<float>& trkjet_R, const ROOT::VecOps::RVec<float>& trkjet_muon_dR);

  //! List of VR jets associated to the probe jet
  /**
   * Includes
   *  - 10 GeV cut on track jets
   *
   * \param probe_fatjet_idx index of fatjet in fatjet lists
   * \param fatjet_trkjet_idxs list of track jets associated to fat jets to be slimmed down
   * \param trkjet_pt list of track jet pT's
   *
   * \return list of idx's of track jets associated to the probe jet
   */
  std::vector<uint32_t> probe_vrjets(uint32_t tag_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<float>& trkjet_pt);

  // Number of b-tags in the leading jet
  uint32_t tagjet_nbtag(const std::vector<uint32_t>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<bool>& trkjet_btag);

  //! Number of b-tags in the two leading jets
  /**
   * If jet has less than two track jets, only those are considered.
   *
   * \param fatjet_trkjet_idxs track jets associated to the probe jet
   * \param trkjet_btag b-tagging decision on all track jets in the event
   *
   * \return Number of b-tags in the three leading track jets
   */
  uint32_t probejet_nbtag(const std::vector<uint32_t>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<bool>& trkjet_btag);
  
  //! product of btag SFs
  double btagSF(const std::vector<uint32_t>& tagjet_trkjet_idx, const std::vector<uint32_t>& probejet_trkjet_idx, const std::vector<std::vector<float>>& trkjet_SF, uint32_t trkjet_SF_idx=0);

  //! if pT < 450 in MC, returns a weight from polynomial fit or srs/crttbar pT spectrum
  float get_poly_reweight(const std::vector<float>& fatjet_pt, const uint32_t idx);
  
  //! if pT < 450 in data, randomly drops events using accept/reject method with polynomial as probability distribution
  bool random_toss(const std::vector<float>& fatjet_pt, const uint32_t idx);
};
#endif // CRTTBARDEFINES_H_
