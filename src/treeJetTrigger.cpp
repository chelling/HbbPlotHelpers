#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <getopt.h>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "JetTriggerAnalysis.h"

// -- Options --
uint32_t nthreads=0;
bool btagging_syst=false;
//

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] outDir" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -t, --threads  Number of threads (default: " << nthreads << ")" << std::endl;
  std::cerr << "" << std::endl;
}

void run_trigger_year(const std::string& year, const std::string& reftrigger, const std::vector<std::string>& triggers,
		      SampleFactory& sf, const std::vector<std::string>& saves, const std::vector<std::string>& branchList, const std::string& outDir)
{
  for(const std::string& save : saves)
    {
      std::cout << "\tProcessing " << save << std::endl;
      if(!sf.contains(save))
	{
	  std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
	  continue;
	}

      JetTriggerAnalysis a(sf[save]->dataframe(), sf[save]->isMC(), reftrigger);

      // Add triggers to branch list
      std::vector<std::string> yearBranchList=branchList;
      for(const std::string& trigger : triggers)
	{
	  yearBranchList.push_back("trigger_"+trigger);
	  yearBranchList.push_back("trigger_isPassBits_"+trigger);
	}

      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
      a.df_reftrigger->Snapshot("outTree",outDir+"/trigger"+year+"_"+save+".root",yearBranchList);
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      std::cout << "\t\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms" << std::endl;
    }
}

int main(int argc, char* argv[])
{
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
        {
          {"threads",    required_argument, 0,  't' },
          {0,          0,              0,  0 }
        };

      c = getopt_long(argc, argv, "t:", long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 't':
          nthreads = std::stoi(optarg);
          break;
        default:
          std::cerr << "Invalid option supplied. Aborting." << std::endl;
          std::cerr << std::endl;
          usage(argv);
	  return 1;
        }
    }

  if (optind == argc)
    {
      std::cerr << "ERROR: Required outDir argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  // Parse arguments
  std::string outDir=argv[optind++];

  // Process stuff
  ROOT::EnableImplicitMT(nthreads);  
  SampleFactory sf("../samples.yml");

  // What to save
  std::vector<std::string> saves15={"data15","Pythia8_dijet.mc16a"};
  std::vector<std::string> saves16={"data16","Pythia8_dijet.mc16a"};
  std::vector<std::string> saves17={"data17","Pythia8_dijet.mc16d"};
  std::vector<std::string> saves18={"data18","Pythia8_dijet.mc16e"};

  std::vector<std::string> triggers15={"HLT_j360_a10_lcw_sub_L1J100","HLT_j360_a10r_L1J100"};
  std::vector<std::string> triggers16={"HLT_j420_a10_lcw_L1J100","HLT_j420_a10r_L1J100"};
  std::vector<std::string> triggers17={"HLT_j390_a10t_lcw_jes_30smcINF_L1J100","HLT_j420_a10t_lcw_jes_40smcINF_L1J100","HLT_j440_a10_lcw_subjes_L1J100","HLT_j440_a10r_L1J100","HLT_j440_a10t_lcw_jes_L1J100","HLT_j460_a10_lcw_subjes_L1J100","HLT_j460_a10r_L1J100","HLT_j460_a10t_lcw_jes_L1J100"};
  std::vector<std::string> triggers18={"HLT_j460_a10_lcw_subjes_L1J100","HLT_j460_a10r_L1J100","HLT_j460_a10t_lcw_jes_L1J100",
				       "HLT_j460_a10_lcw_subjes_L1SC111","HLT_j460_a10r_L1SC111","HLT_j460_a10t_lcw_jes_L1SC111",
				       "HLT_j420_a10t_lcw_jes_30smcINF_L1J100","HLT_j420_a10t_lcw_jes_30smcINF_L1SC111","HLT_j420_a10t_lcw_jes_35smcINF_L1J100","HLT_j420_a10t_lcw_jes_35smcINF_L1SC111"};
  
  std::vector<std::string> branchList={"fatjet0_pt", "fatjet0_m", "fatjet0_muonCorrected_pt", "fatjet0_muonCorrected_m", "fatjet0_MinJ","w"};

  std::cout << "2015" << std::endl;
  run_trigger_year("15","HLT_j110"                   ,triggers15,sf,saves15,branchList,outDir);

  std::cout << "2016" << std::endl;
  run_trigger_year("16","HLT_j260_a10_lcw_L1J75"     ,triggers16,sf,saves16,branchList,outDir);

  std::cout << "2017" << std::endl;
  run_trigger_year("17","HLT_j260_a10t_lcw_jes_L1J75",triggers17,sf,saves17,branchList,outDir);

  std::cout << "2018" << std::endl;
  run_trigger_year("18","HLT_j260_a10t_lcw_jes_L1J75",triggers18,sf,saves18,branchList,outDir);
}
