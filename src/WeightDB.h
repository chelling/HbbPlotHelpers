#ifndef WEIGHTDB_H
#define WEIGHTDB_H

#include <string>
#include <unordered_map>

class WeightDB
{
public:
  WeightDB();

  void scanFile(uint32_t dsid, const std::string& path);
  uint32_t systIdx(uint32_t dsid, const std::string& systName) const;

  /**
   * \brief Get DSID to weight index map for a given systematic
   *
   * \param systName name of the systematic to query
   *
   * \return map with dsid key and value as the index. Empty map if the systematic is not found
   */
  const std::unordered_map<uint32_t, uint32_t>& systIdx(const std::string& systName) const;

private:
  // db[systName][dsid]=systIdx
  std::unordered_map<std::string, std::unordered_map<uint32_t, uint32_t>> m_db;

  static const std::unordered_map<uint32_t, uint32_t> empty;
};

#endif // WEIGHTDB_H
