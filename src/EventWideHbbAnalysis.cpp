#include "EventWideHbbAnalysis.h"

#include "Sample.h"
#include "Defines.h"
#include "EventWideDefines.h"

EventWideHbbAnalysis::EventWideHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst,
					   const std::string& br_truth, const std::string& br_truthTag, const std::string& btagWP, const std::unordered_map<uint32_t, std::vector<double>>& SumW,
					   const std::string& reweightMapName, const std::string& ewCorrections)
  :
  m_isMC(isMC),m_syst(syst),
  m_br_truth(br_truth), m_br_truthTag(br_truthTag), m_btagWP(btagWP), m_SumW(SumW),
  m_reweightMapName(reweightMapName), m_ewCorrections(ewCorrections)
{
  //
  // Load information

  // V+jets
  m_weightdb.scanFile(700040, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700041, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700042, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700043, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700044, "../data/weightdb/Sh228.txt");
  m_weightdb.scanFile(700045, "../data/weightdb/Sh228.txt");

  // ttbar
  m_weightdb.scanFile(410470, "../data/weightdb/PowPy8_ttbar.txt");
  m_weightdb.scanFile(410471, "../data/weightdb/PowPy8_ttbar.txt");

  //
  // Load reweighting maps
  LoadReweightMaps();

  //
  // Create the RDF graph

  df_root=rootdf;

  // Definitions of four-momenta
  df_defp4=std::make_shared<ROOT::RDF::RNode>((*df_root)
					      .Define("fatjet"+syst+"_p4", PtEtaPhiM, {"nfatjet"+syst,"fatjet"+syst+"_pt","fatjet"+syst+"_eta","fatjet"+syst+"_phi","fatjet"+syst+"_m"})
					      .Define("trkjet_p4", PtEtaPhiE, {"ntrkjet","trkjet_pt","trkjet_eta","trkjet_phi","trkjet_E"})
					      .Define("muon_p4", PtEtaPhiM, {"nmuon","muon_pt","muon_eta","muon_phi","muon_m"})
					      );

  if(isMC)
    df_deftr=std::make_shared<ROOT::RDF::RNode>((*df_defp4)
						.Define("truth_pt" , [](float truth_pt )->float { return truth_pt;  }, {br_truth+"_pt" })
						.Define("truth_eta", [](float truth_eta)->float { return truth_eta; }, {br_truth+"_eta"})
						.Define("truth_phi", [](float truth_phi)->float { return truth_phi; }, {br_truth+"_phi"})
                                                .Define("truthjet_p4", PtEtaPhiM, {"ntruthfatjet","truthfatjet_pt","truthfatjet_eta","truthfatjet_phi","truthfatjet_m"})
						);
  else
    df_deftr=df_defp4;

  df_prw=std::make_shared<ROOT::RDF::RNode>((*df_deftr)
					    .Define("weight_prw",[](float weight, float weight_pileup)->float { return weight*weight_pileup; }, {"weight_norm","weight_pileup"})
					    );

  // Calculate EW corrections using an external file
  if(isMC)
    {
      df_prw=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						.Define("ewcorr", [this](int mcChannelNumber, float truth_pt, const std::vector<float>& mcEventWeights)->float
							{ return this->m_ewCorrections.calcEWCorrection(mcChannelNumber,truth_pt,mcEventWeights); },
							{"mcChannelNumber", "truth_pt", "mcEventWeights"})
						.Define("weight_ew", [](float weight, float correction)->float { return weight*correction; }, {"weight_prw", "ewcorr"})
						);
    }

  // trigger
  if(isMC)
    df_trigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						  .Filter(EventWide::trigger_mc,
							  {
							    "runNumber",
							      "trigger_HLT_j360_a10_lcw_sub_L1J100", // 2015
							      "trigger_HLT_j420_a10_lcw_L1J100", // 2016
							      "trigger_HLT_j390_a10t_lcw_jes_30smcINF_L1J100", "trigger_HLT_j440_a10t_lcw_jes_L1J100", // 2017
							      "trigger_HLT_j420_a10t_lcw_jes_35smcINF_L1J100", "trigger_HLT_j460_a10t_lcw_jes_L1J100" // 2018
							      }, "Trigger")
						  .Define("weight_trigger", [](int runNumber, float weight) -> float {
						      switch(runNumber)
							{
							case 284500: return 36.2e3*weight; //mc16a
							case 300000: return 41.0e3*weight; //mc16d
							case 310000: return 58.5e3*weight; //mc16e
							}
						      return 1.;
						    }, {"runNumber", "weight_ew"})
						  );
  else
    df_trigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						  .Filter(EventWide::trigger_data,
							  {
							    "runNumber",
							      "trigger_HLT_j360_a10_lcw_sub_L1J100", // 2015
							      "trigger_HLT_j420_a10_lcw_L1J100", // 2016
							      "trigger_HLT_j390_a10t_lcw_jes_30smcINF_L1J100", "trigger_HLT_j440_a10t_lcw_jes_L1J100", // 2017
							      "trigger_HLT_j420_a10t_lcw_jes_35smcINF_L1J100", "trigger_HLT_j460_a10t_lcw_jes_L1J100" // 2018
							      }, "Trigger")
						  .Define("weight_trigger", []() -> float { return 1.; })
						  );

  df_triggerjet=std::make_shared<ROOT::RDF::RNode>((*df_trigger)
						   .Filter([](const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m) -> bool { return trigger_jet_ptmass(fatjet_pt,fatjet_m,450,60); }, {"fatjet"+syst+"_pt","fatjet"+syst+"_m"} , "Trigger Jet")
						   );

  df_cleaning=std::make_shared<ROOT::RDF::RNode>((*df_triggerjet)
						 .Filter(filter_flag, {"eventClean_LooseBad"} , "Jet Cleaning")
						 );

  df_dijet=std::make_shared<ROOT::RDF::RNode>((*df_cleaning)
					      .Filter([](int32_t nfatjet, const std::vector<float>& fatjet_pt)->bool { return nfatjet>=2 && fatjet_pt[1]>200; }, {"nfatjet"+syst,"fatjet"+syst+"_pt"})
					      .Define("fatjet0"+syst+"_m", [](const std::vector<float>& fatjet_m)->float { return fatjet_m[0]; }, {"fatjet"+syst+"_m"})
					      .Define("fatjet1"+syst+"_m", [](const std::vector<float>& fatjet_m)->float { return fatjet_m[1]; }, {"fatjet"+syst+"_m"})
					      );

  df_trkJet5Idx =std::make_shared<ROOT::RDF::RNode>((*df_dijet     ).Define("fatjet"+syst+"_trkJets5Idx_GhostVR30Rmax4Rmin02TrackJet" , fatjet_trkjet5_idxs ,
  									    {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"}));
  df_trkJet10Idx=std::make_shared<ROOT::RDF::RNode>((*df_trkJet5Idx).Define("fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", fatjet_trkjet10_idxs,
  									    {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"}));

    // Fat jet list of possible cadidates
  df_fatjet_ntj  =std::make_shared<ROOT::RDF::RNode>((*df_trkJet10Idx)
						     .Define("fatjet"+syst+"_ntj", count_subjet, {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet"})
						     );

  df_fatjet_nbin2=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_ntj)
						     .Define("fatjet"+syst+"_nbin2", count_bjetsin2, {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_is_"+btagWP})
						     );
  
  df_fatjet_vrolap=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_nbin2)
						      .Define("trkjet_R", trackjet_VR, {"trkjet_pt"})
						      .Define("fatjet"+syst+"_VR_overlap", fatjet_VR_overlap, {"fatjet"+syst+"_trkJets5Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_p4","trkjet_R"})
						      );


  df_fatjet_boost=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_vrolap)
						     .Define("fatjet"+syst+"_boost",fatjet_boost,{"fatjet"+syst+"_pt","fatjet"+syst+"_m"})
						     );

  df_fatjet_inlist=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_boost)
						      .Define("fatjet"+syst+"_inlist",
							      [](const ROOT::VecOps::RVec<float>& fatjet_pt, const ROOT::VecOps::RVec<float>& fatjet_m, const ROOT::VecOps::RVec<float>& fatjet_boost, const ROOT::VecOps::RVec<uint32_t>& fatjet_ntj, const ROOT::VecOps::RVec<bool>& fatjet_VR_overlap)->ROOT::VecOps::RVec<bool>
							      { return (fatjet_ntj>=2)&&(fatjet_VR_overlap==false)&&(fatjet_pt>250)&&(fatjet_m>=60)&&(fatjet_boost<1); },
							      {"fatjet"+syst+"_pt","fatjet"+syst+"_m","fatjet"+syst+"_boost","fatjet"+syst+"_ntj","fatjet"+syst+"_VR_overlap"})
						      .Filter([](const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool { return fatjet_inlist[0] || fatjet_inlist[1]; },
							      {"fatjet"+syst+"_inlist"}, "Fat Jet List")
						      );

  // B-tagging
  df_category =std::make_shared<ROOT::RDF::RNode>((*df_fatjet_inlist)
						  .Define("SRL",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[0] && fatjet_nbin2[0]==2; },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Define("SRS",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[1] && fatjet_nbin2[1]==2 && !(fatjet_inlist[0] && fatjet_nbin2[0]==2); },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Define("VRL",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[0] && fatjet_nbin2[0]==0 && !(fatjet_inlist[1] && (fatjet_nbin2[1]==1 || fatjet_nbin2[1]==2)); },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Define("VRS",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[1] && fatjet_nbin2[1]==0 && !(fatjet_inlist[0] && (fatjet_nbin2[0]==1 || fatjet_nbin2[0]==2)); },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Filter(filter_or, {"SRL","SRS","VRL","VRS"}, "Category")
						  );

  if(isMC)
    {
      df_truth  =std::make_shared<ROOT::RDF::RNode>((*df_category)
						    .Define("fatjet"+syst+"_nTruthB", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(  5, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    .Define("fatjet"+syst+"_nTruthC", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(  4, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    .Define("fatjet"+syst+"_nTruthL", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(  0, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    .Define("fatjet"+syst+"_nTruthX", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(-99, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    );
      
      df_srl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("SRL","SRL")
								 ),0,syst);

      df_srs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("SRS","SRS")
								 ),1,syst);

      df_vrl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("VRL","VRL")
								 ),0,syst);

      df_vrs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("VRS","VRS")
								 ),1,syst);
      
      // b-tagging systematics loop
      const std::vector<std::string> the_btagging_systematics=( syst.empty() && btagging_systematics.count(btagWP)!=0 ) ? btagging_systematics.at(btagWP) : std::vector<std::string>({""});

      for(uint32_t btag_syst_idx=0;
	  btag_syst_idx<the_btagging_systematics.size();
	  btag_syst_idx++)
	{
	  std::string bsystname=the_btagging_systematics[btag_syst_idx];

	  df_srl_inc=std::make_shared<ROOT::RDF::RNode>((*df_srl_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    return weight*
							      trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx];
							  }, {"weight_trigger","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_"+btagWP})
							);

	  df_srs_inc=std::make_shared<ROOT::RDF::RNode>((*df_srs_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const ROOT::VecOps::RVec<bool>& fatjet_inlist, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    if(fatjet_inlist[0]) // Had to run b-tagging on the leading jet
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx]*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							    else
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							  },{"weight_trigger","fatjet"+syst+"_inlist","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_"+btagWP})
							);

	  df_vrl_inc=std::make_shared<ROOT::RDF::RNode>((*df_vrl_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const ROOT::VecOps::RVec<bool>& fatjet_inlist, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    if(fatjet_inlist[1]) // Had to run b-tagging on the leading and subleading jet
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx]*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							    else
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx];
							  },{"weight_trigger","fatjet"+syst+"_inlist","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_"+btagWP})
							);

	  df_vrs_inc=std::make_shared<ROOT::RDF::RNode>((*df_vrs_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const ROOT::VecOps::RVec<bool>& fatjet_inlist, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    if(fatjet_inlist[0]) // Had to run b-tagging on the leading and subleading jet
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx]*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							    else
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							  },{"weight_trigger","fatjet"+syst+"_inlist","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_"+btagWP})
							);
	}

      // Extra weights
      df_srl_inc=Define_Weights(df_srl_inc);
      df_srs_inc=Define_Weights(df_srs_inc);
      df_vrl_inc=Define_Weights(df_vrl_inc);
      df_vrs_inc=Define_Weights(df_vrs_inc);
    }
  else
    {
      df_srl_inc=std::make_shared<ROOT::RDF::RNode>(
						    Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
												    .Filter("SRL")
												    .Define("w", []()->float { return 1.; })
												    ),0,syst)
						    ->Filter([](float Hcand_m)->bool { return Hcand_m<105 || 145<Hcand_m; },{"Hcand_m"}) // Blinding
						    );

      df_srs_inc=std::make_shared<ROOT::RDF::RNode>(
						    Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
												    .Filter("SRS")
												    .Define("w", []()->float { return 1.; })
												    ),1,syst)
						    ->Filter([](float Hcand_m)->bool { return Hcand_m<100 || 145<Hcand_m; },{"Hcand_m"}) // Blinding
						    );

      df_vrl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
								 .Filter("VRL")
								 .Define("w", []()->float { return 1.; })
								 ),0,syst);

      df_vrs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
								 .Filter("VRS")
								 .Define("w", []()->float { return 1.; })
								 ),1,syst);
    }
}

EventWideHbbAnalysis::~EventWideHbbAnalysis()
{
  // Close configuration files
  if(m_fMaps)
    {
      m_fMaps->Close();
      m_fMaps=nullptr;
    }
}

std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::Define_Hcand(std::shared_ptr<ROOT::RDF::RNode> df, uint32_t Hcand_idx, const std::string& syst)
{
  // Hcand definitions common to both data and MC
  std::shared_ptr<ROOT::RDF::RNode> df_tmp = std::make_shared<ROOT::RDF::RNode>((*df)
										.Define("Hcand_idx"       , [Hcand_idx]()->uint32_t { return Hcand_idx; })
										.Define("Hcand_pt"        , define_idx<float>, { "fatjet"+syst+"_muonCorrected_pt" , "Hcand_idx" })
										.Define("Hcand_m"         , define_idx<float>, { "fatjet"+syst+"_muonCorrected_m"  , "Hcand_idx" })
										.Define("Hcand_eta"       , define_idx<float>, { "fatjet"+syst+"_muonCorrected_eta", "Hcand_idx" })
										.Define("Hcand_phi"       , define_idx<float>, { "fatjet"+syst+"_muonCorrected_phi", "Hcand_idx" })
										.Define("Hcand_uncorr_pt" , define_idx<float>, { "fatjet"+syst+"_pt"               , "Hcand_idx" })
										.Define("Hcand_uncorr_m"  , define_idx<float>, { "fatjet"+syst+"_m"                , "Hcand_idx" })
										.Define("Hcand_uncorr_eta", define_idx<float>, { "fatjet"+syst+"_eta"              , "Hcand_idx" })
										.Define("Hcand_uncorr_phi", define_idx<float>, { "fatjet"+syst+"_phi"              , "Hcand_idx" })
										);

  if(syst=="")
    {
      df_tmp = std::make_shared<ROOT::RDF::RNode>((*df_tmp)
						  .Define("Hcand_C2"                 , define_idx<float>, { "fatjet"+syst+"_C2"              , "Hcand_idx" })
						  .Define("Hcand_trkjet0_isMV2c10_85", fatjet_trkjet_tjidx<int,0>,
							  { "fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_is_MV2c10_FixedCutBEff_85", "Hcand_idx"})
						  .Define("Hcand_trkjet1_isMV2c10_85", fatjet_trkjet_tjidx<int,1>,
							  { "fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_is_MV2c10_FixedCutBEff_85", "Hcand_idx"})
						  );
    }

  if(m_isMC && syst=="")
    {
      df_tmp = std::make_shared<ROOT::RDF::RNode>((*df_tmp)
						  .Define("Hcand_nTruthBosons" , define_idx<int>  , { "fatjet"+syst+"_"+m_br_truthTag  , "Hcand_idx" })
						  .Define("Hcand_nTruthB"      , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB )->uint32_t { return fatjet_nTruthB[Hcand_idx]; },{"fatjet"+syst+"_nTruthB" })
						  .Define("Hcand_nTruthC"      , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthC )->uint32_t { return fatjet_nTruthC[Hcand_idx]; },{"fatjet"+syst+"_nTruthC" })
						  .Define("Hcand_nTruthL"      , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthL )->uint32_t { return fatjet_nTruthL[Hcand_idx]; },{"fatjet"+syst+"_nTruthL" })
						  .Define("Hcand_nTruthX"      , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthX )->uint32_t { return fatjet_nTruthX[Hcand_idx]; },{"fatjet"+syst+"_nTruthX" })
                                                  .Define("category"     , [Hcand_idx](const uint32_t Hcand_nTruthB)->std::string {
                                                                                       if(Hcand_nTruthB>=2) return "bb";
                                                                                       else if(Hcand_nTruthB==1) return "bx";
                                                                                       else return "xx";
                                                                                       },{"Hcand_nTruthB"})
						  );

      df_tmp = std::make_shared<ROOT::RDF::RNode>((*df_tmp)
						  .Define("Hcand_truth_idx" , matchdR_idx, {"Hcand_pt","Hcand_eta","Hcand_phi","Hcand_m","truthjet_p4"})
						  .Define("Hcand_truth_pt"  , define_idx<float>, {"truthfatjet_pt","Hcand_truth_idx"})
						  .Define("Hcand_truth_m"   , define_idx<float>, {"truthfatjet_m" ,"Hcand_truth_idx"})
						  );

      if(!m_reweightMapName.empty())
        {
          df_tmp = std::make_shared<ROOT::RDF::RNode>((*df_tmp)
                                                      .Filter([](const std::vector<float> truthfatjet_pt)->bool {return truthfatjet_pt.size()>0;},{"truthfatjet_pt"})
						      .Define("reweight", [this,Hcand_idx](float Hcand_truth_m,
                                                                                           float Hcand_truth_pt,
                                                                                           const std::string& category)->float
                                                                          {
                                                                           std::string key;
                                                                           if (Hcand_idx == 0) key = category+"l";
                                                                           else if (Hcand_idx == 1) key = category+"s";
                                                                           return getReweight(this->m_sysMaps[key],Hcand_truth_m,Hcand_truth_pt);
                                                                          } , {"Hcand_truth_m","Hcand_truth_pt","category"})
						      );
        }
    }

  return df_tmp;  
}

std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::Define_Weights(std::shared_ptr<ROOT::RDF::RNode> df)
{
  //
  // Alternate weights
  for(const std::string& wname : extra_weights)
    {
      const std::unordered_map<uint32_t, uint32_t>& systidxmap=m_weightdb.systIdx(wname);

      df=std::make_shared<ROOT::RDF::RNode>((*df)
					    .Define("w"+wname, [this,systidxmap](int runNumber, int mcChannelNumber, float weight, const std::vector<float>& mcEventWeights)->float {
								 if(systidxmap.count(mcChannelNumber)==1 && this->m_SumW.count(dskey(runNumber,mcChannelNumber))==1)
								   {
								     uint32_t systidx=systidxmap.at(mcChannelNumber);
								     const std::vector<double>& sumw=this->m_SumW.at(dskey(runNumber,mcChannelNumber));
								     return weight*
								       (mcEventWeights.at(systidx)/sumw.at(systidx))/
								       (mcEventWeights.at(      0)/sumw.at(      0));
								   }
								 else
								   return weight;
							       },
					      {"runNumber", "mcChannelNumber", "w", "mcEventWeights"}
					      )
					    );
    }

  //
  // Remove EW corrections
  df=std::make_shared<ROOT::RDF::RNode>((*df)
					.Define("wNOEW", [](float weight, float ewcorr)->float
						{ return weight/ewcorr; },
						{"w", "ewcorr"}
						)
					);

  return df;
}

void EventWideHbbAnalysis::LoadReweightMaps()
{
  // Load 2D maps
  if(!m_reweightMapName.empty())
    {
      m_fMaps = TFile::Open(m_reweightMapName.c_str());
      std::vector<std::string> regions = {"bbl","bbs","bxl","bxs","xxl","xxs"};
      for(std::string reg : regions)
        {
         std::string mapName = "hMap_"+reg; 
         m_sysMaps.insert(std::pair<std::string, TH2*>(reg, m_fMaps->Get<TH2>(mapName.data())));
        } 
    }
}
