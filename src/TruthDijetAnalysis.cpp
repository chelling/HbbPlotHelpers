#include "TruthDijetAnalysis.h"

#include "Defines.h"

TruthDijetAnalysis::TruthDijetAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& reweightMapName)
  : m_isMC(isMC), m_reweightMapName(reweightMapName)
{
  df_root=rootdf;

  // Load reweighting maps
  LoadReweightMaps();

  // Definitions of weights
  if(isMC)
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root) 
                                                   .Define("w", [](float weight)->double
                                                           { return weight*(36.2e3+41.0e3+58.5e3); },
                                                           {"weight_norm"})
                                                   );
    }
  else
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root)
                                                   .Define("w", []()->double { return 1.; })
                                                   );
    }

  // Jet selection
  df_dijet=std::make_shared<ROOT::RDF::RNode>((*df_weight)
                                              .Filter([](const int32_t nfatjet)->bool {return nfatjet>=2;},{"nfatjet"})
                                              .Filter([](const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m) -> bool 
                                                         { return trigger_jet_ptmass(fatjet_pt,fatjet_m,450,60); }, {"fatjet_pt","fatjet_m"})
                                              .Filter([](const std::vector<float>& fatjet_pt)->bool
                                                         {return fatjet_pt[1]>200;},{"fatjet_pt"}) 
                                              );

  // p4 definitions
  df_defp4=std::make_shared<ROOT::RDF::RNode>((*df_dijet)
                                              .Define("trkjet_p4", PtEtaPhiE, {"ntrkjet","trkjet_pt","trkjet_eta","trkjet_phi","trkjet_E"})
                                              .Define("fatjet_p4", PtEtaPhiM, {"nfatjet","fatjet_pt","fatjet_eta","fatjet_phi","fatjet_m"})
                                              );

  // Good trkjets
  df_good_trkjets=std::make_shared<ROOT::RDF::RNode>((*df_defp4)
                                                    .Define("fatjet_trkJets5Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets", fatjet_trkjet5_idxs,
                                                            {"fatjet_trkJetsIdx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets","trkjet_pt"})
                                                    .Define("fatjet_trkJets10Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets", fatjet_trkjet10_idxs,
                                                            {"fatjet_trkJetsIdx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets","trkjet_pt"})
                                                    );
 
  // Count number of subjets
  df_fatjet_ntj=std::make_shared<ROOT::RDF::RNode>((*df_good_trkjets)
                                                    .Define("fatjet_ntj", count_subjet, {"fatjet_trkJets10Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets"})
                                                    );

  // VR trkjet overlap
  df_fatjet_vrolap=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_ntj)
                                                      .Define("trkjet_R", trackjet_VR, {"trkjet_pt"})
                                                      .Define("fatjet_VR_overlap", fatjet_VR_overlap,
                                                              {"fatjet_trkJets5Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets","trkjet_p4","trkjet_R"})
                                                      );

  // Fatjet boost
  df_fatjet_boost=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_vrolap)
                                                     .Define("fatjet_boost",fatjet_boost,{"fatjet_pt","fatjet_m"})
                                                     );

  // Fatjet inlist
  df_fatjet_inlist=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_boost)
                                                      .Define("fatjet_inlist",
                                                              [](const ROOT::VecOps::RVec<float>& fatjet_pt, const ROOT::VecOps::RVec<float>& fatjet_m, const ROOT::VecOps::RVec<float>& fatjet_boost, const ROOT::VecOps::RVec<uint32_t>& fatjet_ntj, const ROOT::VecOps::RVec<bool>& fatjet_VR_overlap)->ROOT::VecOps::RVec<bool>
                                                              { return (fatjet_ntj>=2)&&(fatjet_VR_overlap==false)&&(fatjet_pt>250)&&(fatjet_m>=60)&&(fatjet_boost<1); },
                                                              {"fatjet_pt","fatjet_m","fatjet_boost","fatjet_ntj","fatjet_VR_overlap"})
                                                      .Filter([](const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool 
                                                              { return fatjet_inlist[0] || fatjet_inlist[1]; }, {"fatjet_inlist"})
                                                      );

  // Truth flavour category
  df_category=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_inlist)
                                                 .Define("fatjet_nTruthB", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
                                                         { return count_truthjets(  5, fatjet_trkjet_idxs, trkjet_truth); },
                                                         {"fatjet_trkJets10Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets","trkjet_HadronConeExclTruthLabelID"})
                                                 .Define("fatjet_nTruthC", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
                                                         { return count_truthjets(  4, fatjet_trkjet_idxs, trkjet_truth); },
                                                         {"fatjet_trkJets10Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets","trkjet_HadronConeExclTruthLabelID"})
                                                 .Define("fatjet_nTruthL", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
                                                         { return count_truthjets(  0, fatjet_trkjet_idxs, trkjet_truth); },
                                                         {"fatjet_trkJets10Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets","trkjet_HadronConeExclTruthLabelID"})
                                                 .Define("fatjet_nTruthX", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
                                                         { return count_truthjets(-99, fatjet_trkjet_idxs, trkjet_truth); },
                                                         {"fatjet_trkJets10Idx_GhostAntiKtVR30Rmax4Rmin02TruthChargedJets","trkjet_HadronConeExclTruthLabelID"})
                                                 .Define("bbl", [](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB, const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool 
                                                         {return fatjet_inlist[0] && fatjet_nTruthB[0]>=2;}, {"fatjet_nTruthB","fatjet_inlist"})
                                                 .Define("bbs", [](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB, const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool
                                                         {return fatjet_inlist[1] && fatjet_nTruthB[1]>=2;}, {"fatjet_nTruthB","fatjet_inlist"})
                                                 .Define("bxl", [](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB, const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool
                                                         {return fatjet_inlist[0] && fatjet_nTruthB[0]==1;}, {"fatjet_nTruthB","fatjet_inlist"})
                                                 .Define("bxs", [](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB, const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool
                                                         {return fatjet_inlist[1] && fatjet_nTruthB[1]==1;}, {"fatjet_nTruthB","fatjet_inlist"})
                                                 .Define("xxl", [](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB, const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool
                                                         {return fatjet_inlist[0] && fatjet_nTruthB[0]==0;}, {"fatjet_nTruthB","fatjet_inlist"})
                                                 .Define("xxs", [](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB, const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool
                                                         {return fatjet_inlist[1] && fatjet_nTruthB[1]==0;}, {"fatjet_nTruthB","fatjet_inlist"}) 
                                                 ); // Note: X accounts for C, L and other

  // Candidate selection
  df_bbl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>(*df_category),0,"bbl");
  df_bbs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>(*df_category),1,"bbs");
  df_bxl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>(*df_category),0,"bxl");
  df_bxs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>(*df_category),1,"bxs");
  df_xxl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>(*df_category),0,"xxl");
  df_xxs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>(*df_category),1,"xxs");
}

std::shared_ptr<ROOT::RDF::RNode> TruthDijetAnalysis::Define_Hcand(std::shared_ptr<ROOT::RDF::RNode> df, uint32_t Hcand_idx, const std::string& category)
{
  std::shared_ptr<ROOT::RDF::RNode> df_tmp = std::make_shared<ROOT::RDF::RNode>((*df) 
                                               .Define("Hcand_idx", [Hcand_idx]()->uint32_t { return Hcand_idx; }) 
                                               .Define("Hcand_m"  , [Hcand_idx](const std::vector<float>& fatjet_m)->float { return fatjet_m[Hcand_idx]; }, {"fatjet_m"})
                                               .Define("Hcand_pt" , [Hcand_idx](const std::vector<float>& fatjet_pt)->float { return fatjet_pt[Hcand_idx]; }, {"fatjet_pt"})
                                               .Filter(category) 
                                             );

  // Reweighting (validation)
  if(!m_reweightMapName.empty())
  {
    df_tmp = std::make_shared<ROOT::RDF::RNode>((*df_tmp)
                                                .Define("reweight", [this,category](const float Hcand_m, const float Hcand_pt)->float {return getReweight(this->m_sysMaps[category],Hcand_m,Hcand_pt); }, {"Hcand_m","Hcand_pt"})
                                                  );
  }
  return df_tmp;
}

void TruthDijetAnalysis::LoadReweightMaps()
{
  if(!m_reweightMapName.empty())
    {
      m_fMaps = TFile::Open(m_reweightMapName.c_str());
      std::vector<std::string> regions = {"bbl","bbs","bxl","bxs","xxl","xxs"};
      for(std::string reg : regions)
        {
         std::string mapName = "hMap_"+reg;
         m_sysMaps.insert(std::pair<std::string, TH2*>(reg, m_fMaps->Get<TH2>(mapName.data())));
        }
    }
}
