#ifndef SAMPLE_H
#define SAMPLE_H

#include <vector>
#include <string>
#include <memory>

#include "ROOT/RDataFrame.hxx"

uint32_t dskey(int runNumber, int mcChannelNumber);

class Sample
{
public:
  Sample() =default;

  void setScale(double scale);
  void setWeightIdx(uint32_t idx);
  void setExtraWeights(const std::vector<uint32_t>& extraWeights);

  virtual bool isMC() const =0;

  std::shared_ptr<ROOT::RDataFrame> root     ();
  std::shared_ptr<ROOT::RDF::RNode> dataframe();

  virtual std::vector       <std::string> filelist() =0;

  virtual std::unordered_map<uint32_t, double               > scales      () =0;
  virtual std::unordered_map<uint32_t, std::vector<  double>> sumW        () =0;
  virtual std::unordered_map<uint32_t, uint32_t             > weightIdx   () =0;
  virtual std::unordered_map<uint32_t, std::vector<uint32_t>> extraWeights() =0;

protected:  
  virtual void init_root();
  std::shared_ptr<ROOT::RDataFrame> m_root=nullptr;

protected:
  double                m_scale          =1.;

  bool                  m_weightIdxSet   =false;
  uint32_t              m_weightIdx;

  bool                  m_extraWeightsSet=false;  
  std::vector<uint32_t> m_extraWeights;
};

#endif // SAMPLE_H
