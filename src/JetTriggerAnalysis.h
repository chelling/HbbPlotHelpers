#ifndef JETTRIGGERANALYSIS_H
#define JETTRIGGERANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class JetTriggerAnalysis
{
public:
  JetTriggerAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& reftrigger);

  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;

  std::shared_ptr<ROOT::RDF::RNode> df_prw;

  std::shared_ptr<ROOT::RDF::RNode> df_reftrigger;
};

#endif // JETTRIGGERANALYSIS_H
