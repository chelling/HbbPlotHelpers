#ifndef WTTBARDEFINES_H_
#define WTTBARDEFINES_H_

#include <stdint.h>

#include <vector>

#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

namespace Wttbar
{

bool muon_isolation(const ROOT::VecOps::RVec<TLorentzVector>& muon_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag);

int closest_bjet(const ROOT::VecOps::RVec<TLorentzVector>& muon_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag);

float closest_bjet_dR(const ROOT::VecOps::RVec<TLorentzVector>& fatjet_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag);

bool outside_bjet(const ROOT::VecOps::RVec<TLorentzVector>& fatjet_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag);

std::vector<int> muon_vrjet_idxs(const ROOT::VecOps::RVec<TLorentzVector>& muon_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const ROOT::VecOps::RVec<float>& trkjet_R, const std::vector<int>& trkjet_btag);

std::vector<int> W_vrjet_idxs(const ROOT::VecOps::RVec<TLorentzVector>& fatjet_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag);

double btagSF(const std::vector<int>& W_vrjet_idxs, const std::vector<int>& muon_vrjet_idxs, const std::vector<std::vector<float>>& trkjet_SF, uint32_t trkjet_SF_idx);

};
#endif // WTTBARDEFINES_H_
