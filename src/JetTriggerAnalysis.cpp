#include "JetTriggerAnalysis.h"

#include "Defines.h"

JetTriggerAnalysis::JetTriggerAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& reftrigger)
{
  df_root=rootdf;

  // Definitions of four-momenta
  df_prw=std::make_shared<ROOT::RDF::RNode>((*df_root)
					    .Filter(filter_flag, {"eventClean_LooseBad"} , "Jet Cleaning")
					    .Define("w",[](float weight, float weight_pileup)->float { return weight*weight_pileup; }, {"weight_norm","weight_pileup"})
					    .Filter([](int32_t nfatjet)->bool { return nfatjet>0; }, {"nfatjet"}, "Fat Jet")
					    .Define("fatjet0_pt",[](const std::vector<float>& fatjet_pt)->float { return fatjet_pt[0]; }, {"fatjet_pt"})
					    .Define("fatjet0_m" ,[](const std::vector<float>& fatjet_m )->float { return fatjet_m [0]; }, {"fatjet_m" })
					    .Define("fatjet0_muonCorrected_pt",[](const std::vector<float>& fatjet_pt)->float { return fatjet_pt[0]; }, {"fatjet_muonCorrected_pt"})
					    .Define("fatjet0_muonCorrected_m" ,[](const std::vector<float>& fatjet_m )->float { return fatjet_m [0]; }, {"fatjet_muonCorrected_m" })
					    .Define("fatjet0_MinJ", [](float fatjet_pt, float fatjet_muonCorrected_pt)->bool
						    { return fabsf(fatjet_pt-fatjet_muonCorrected_pt)>0.1; },
						    {"fatjet0_pt","fatjet0_muonCorrected_pt"})
					    );

  // Nominal selection
  df_reftrigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						   .Filter([](int8_t reftrigger)->bool { return reftrigger==1; }, {"trigger_"+reftrigger} ,"Reference Trigger")
						   );
}
