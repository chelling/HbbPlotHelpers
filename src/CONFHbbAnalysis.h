#ifndef CONFHBBANALYSIS_H
#define CONFHBBANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class CONFHbbAnalysis
{
public:
  CONFHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& syst="");

  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;

  std::shared_ptr<ROOT::RDF::RNode> df_prw;
  std::shared_ptr<ROOT::RDF::RNode> df_trigger;
  std::shared_ptr<ROOT::RDF::RNode> df_cleaning;
  std::shared_ptr<ROOT::RDF::RNode> df_dijet;

  std::shared_ptr<ROOT::RDF::RNode> df_trkJet5Idx;
  std::shared_ptr<ROOT::RDF::RNode> df_trkJet10Idx;
  
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_idx;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_ntj;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_nbin2;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_vrolap;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_boost;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjetlist;
 
  std::shared_ptr<ROOT::RDF::RNode> df_category;

  std::shared_ptr<ROOT::RDF::RNode> df_Hcand;
  std::shared_ptr<ROOT::RDF::RNode> df_sf;

  std::shared_ptr<ROOT::RDF::RNode> df_sr;
  std::shared_ptr<ROOT::RDF::RNode> df_vr;
};

#endif // CONFHBBANALYSIS_H
