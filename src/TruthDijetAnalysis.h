#ifndef TRUTHDIJETANALYSIS_H
#define TRUTHDIJETANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class TruthDijetAnalysis
{
public:
  TruthDijetAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& reweightMapName = "");

  // Nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_weight;
  std::shared_ptr<ROOT::RDF::RNode> df_dijet;
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;
  std::shared_ptr<ROOT::RDF::RNode> df_good_trkjets;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_ntj;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_vrolap;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_boost;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_inlist; 
  std::shared_ptr<ROOT::RDF::RNode> df_category;  

  // BB
  std::shared_ptr<ROOT::RDF::RNode> df_bbl_inc;
  std::shared_ptr<ROOT::RDF::RNode> df_bbs_inc;
  // BX
  std::shared_ptr<ROOT::RDF::RNode> df_bxl_inc;
  std::shared_ptr<ROOT::RDF::RNode> df_bxs_inc;
  // XX
  std::shared_ptr<ROOT::RDF::RNode> df_xxl_inc;
  std::shared_ptr<ROOT::RDF::RNode> df_xxs_inc;
private:
  bool m_isMC = false;
  std::shared_ptr<ROOT::RDF::RNode> Define_Hcand(std::shared_ptr<ROOT::RDF::RNode> df, uint32_t Hcand_idx, const std::string& category);

  // Reweighting (validation)
  TFile* m_fMaps=nullptr;
  std::string m_reweightMapName = "";
  std::map<std::string,TH2*> m_sysMaps;
  void LoadReweightMaps();
};

#endif // TRUTHDIJETANALYSIS_H
