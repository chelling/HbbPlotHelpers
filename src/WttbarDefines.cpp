#include <chrono>
#include "WttbarDefines.h"

namespace Wttbar
{

// returns T/F if muon is isolated from a btagged jet
bool muon_isolation(const ROOT::VecOps::RVec<TLorentzVector>& muon_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag)
{
  bool isolated = true;
  TLorentzVector muon = muon_tlv[0];

  for (int i = 0; i < trkjet_tlv.size(); ++i)
  {
    if (muon.DeltaR(trkjet_tlv[i]) < 0.4 && trkjet_tlv[i].Pt() > 10.0) 
		{
			if (trkjet_btag[i]) return false;
		}
  }

  return isolated;
}

int closest_bjet(const ROOT::VecOps::RVec<TLorentzVector>& muon_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag)
{
  int bjet_idx = -99;
  float dR_min = 99;
  TLorentzVector muon = muon_tlv[0];

  for (int i = 0; i < trkjet_tlv.size(); ++i)
  {
    float dR = muon.DeltaR(trkjet_tlv[i]);
    if (dR < 1.5 && dR < dR_min && trkjet_tlv[i].Pt() > 25.0)
      {
        if (trkjet_btag[i])
        {
          dR_min = dR;
          bjet_idx = i;
        }
      }
  }

  return bjet_idx;
}

float closest_bjet_dR(const ROOT::VecOps::RVec<TLorentzVector>& fatjet_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag)
{

  float dR_min = 99;
  TLorentzVector fatjet = fatjet_tlv[0];

  for (int i = 0; i < trkjet_tlv.size(); ++i)
  {
    float dR = fatjet.DeltaR(trkjet_tlv[i]);
    if (trkjet_tlv[i].Pt() > 10.0 and dR < dR_min)
    {
      if (trkjet_btag[i]) dR_min = dR;
    }
  }
  return dR_min;
}

bool outside_bjet(const ROOT::VecOps::RVec<TLorentzVector>& fatjet_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag)
{
	TLorentzVector fatjet = fatjet_tlv[0];

	for (int i = 0; i < trkjet_tlv.size(); ++i)
	{
		float dR = fatjet.DeltaR(trkjet_tlv[i]);
		if (trkjet_tlv[i].Pt() > 10.0 and (dR > 1.0 and dR < 1.5))
		{
			if (trkjet_btag[i]) return true;
		}
	}
	return false;		
}

std::vector<int> muon_vrjet_idxs(const ROOT::VecOps::RVec<TLorentzVector>& muon_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const ROOT::VecOps::RVec<float>& trkjet_R, const std::vector<int>& trkjet_btag)
{
  TLorentzVector muon = muon_tlv[0];
	std::vector<int> vrjet_idxs;
  
	for (int i = 0; i < trkjet_tlv.size(); ++i)
  {
    float dR = muon.DeltaR(trkjet_tlv[i]);
    if (dR < 1.5 && dR > trkjet_R[i] && trkjet_tlv[i].Pt() > 25.0)
      {
        if (trkjet_btag[i])
        {
          vrjet_idxs.push_back(i);
					return vrjet_idxs;
          
        }else
					vrjet_idxs.push_back(i);
      }
  }

	// if you haven't returned vrjet_idxs by this point, there are no btagged jets.
	// push_back a -99 to filter out the event in the next step.
	vrjet_idxs.push_back(-99);

	return vrjet_idxs;

}

std::vector<int> W_vrjet_idxs(const ROOT::VecOps::RVec<TLorentzVector>& fatjet_tlv, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_tlv, const std::vector<int>& trkjet_btag)
{
	TLorentzVector fatjet = fatjet_tlv[0];
	std::vector<int> vrjet_idxs;

	for (int i = 0; i < trkjet_tlv.size(); ++i)
	{
		float dR = fatjet.DeltaR(trkjet_tlv[i]);
		if (trkjet_tlv[i].Pt() > 10.0 and (dR > 1.0 and dR < 1.5))
		{
			if (trkjet_btag[i])
			{
				vrjet_idxs.push_back(i);
				return vrjet_idxs;
			}else
				vrjet_idxs.push_back(i);
		}
	}
 
	// if you haven't returned vrjet_idxs by this point, there are no btagged jets.
	// push_back a -99 to filter out the event in the next step.
	vrjet_idxs.push_back(-99);

	return vrjet_idxs;
}

// returns SF from b-tags
double btagSF(const std::vector<int>& W_vrjet_idxs, const std::vector<int>& muon_vrjet_idxs, const std::vector<std::vector<float>>& trkjet_SF, uint32_t trkjet_SF_idx)
{
  double SF = 1.0;
  // Get SF(s) from bjet near W fatjet
  for (uint32_t j = 0; j < W_vrjet_idxs.size(); ++j)
    SF *= trkjet_SF[W_vrjet_idxs[j]][trkjet_SF_idx];
  // Get SF(s) from bjet near muon 
  for (uint32_t j = 0; j < muon_vrjet_idxs.size(); ++j)
    SF *= trkjet_SF[muon_vrjet_idxs[j]][trkjet_SF_idx];

  return SF;
}


}
