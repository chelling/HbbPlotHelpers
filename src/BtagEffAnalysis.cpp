#include "BtagEffAnalysis.h"

#include "Defines.h"
#include "EventWideDefines.h"

#include <functional>

BtagEffAnalysis::BtagEffAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, const std::string& btagWP)
{
  df_root  =std::make_shared<ROOT::RDF::RNode>((*rootdf)
						  .Define("w", [](int runNumber, float weight) -> float {
						      switch(runNumber)
							{
							case 284500: return 36.2e3*weight; //mc16a
							case 300000: return 41.0e3*weight; //mc16d
							case 310000: return 58.5e3*weight; //mc16e
							}
						      return 1.;
						    }, {"runNumber", "weight_norm"})
					       );

  df_fatjet=std::make_shared<ROOT::RDF::RNode>((*df_root)
					       .Filter([](int32_t nfatjet)->bool { return nfatjet>0; }, {"nfatjet"} )
					       .Define("fatjet0_pt", define_float_idx0, {"fatjet_pt"})
					       );

  df_trkjet=std::make_shared<ROOT::RDF::RNode>((*df_fatjet)
					       .Define("fatjet_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", fatjet_trkjet10_idxs,
						       {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"})
					       .Define("fatjet0_ntrkjet", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs)->uint32_t { return fatjet_ntrkjet(0,fatjet_trkjet_idxs); },
						       {"fatjet_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet"})
					       .Filter([](uint32_t fatjet_ntrkjet)->bool { return fatjet_ntrkjet>=2; }, {"fatjet0_ntrkjet"})
					       .Define("fatjet0_trkjet0_olap", fatjet_trkjet_olap_idx<0, 0>,
						       {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_pt", "trkjet_eta", "trkjet_phi"})
					       .Define("fatjet0_trkjet1_olap", fatjet_trkjet_olap_idx<0, 1>,
						       {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_pt", "trkjet_eta", "trkjet_phi"})
					       .Filter([](bool trkjet0_olap, bool trkjet1_olap)->bool { return !trkjet0_olap && !trkjet1_olap; }, {"fatjet0_trkjet0_olap", "fatjet0_trkjet1_olap"})
					       );

  df_def   =std::make_shared<ROOT::RDF::RNode>((*df_trkjet)
					       .Define("fatjet0_trkjet0_pt"  , fatjet_trkjet_idx<float  ,0,0>, {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_pt"})
					       .Define("fatjet0_trkjet1_pt"  , fatjet_trkjet_idx<float  ,0,1>, {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_pt"})
					       .Define("fatjet0_trkjet0_btag", fatjet_trkjet_idx<int32_t,0,0>, {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_is_"+btagWP})
					       .Define("fatjet0_trkjet1_btag", fatjet_trkjet_idx<int32_t,0,1>, {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_is_"+btagWP})
					       .Define("fatjet0_trkjet0_flav", fatjet_trkjet_idx<int32_t,0,0>, {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_HadronConeExclTruthLabelID"})
					       .Define("fatjet0_trkjet1_flav", fatjet_trkjet_idx<int32_t,0,1>, {"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_HadronConeExclTruthLabelID"})
					       );
 
  for(uint32_t btag_syst_idx=0;
      btag_syst_idx<btagging_systematics.at(btagWP).size();
      btag_syst_idx++)
    {
      const std::string& bsystname=btagging_systematics.at(btagWP)[btag_syst_idx];
      std::cout << btag_syst_idx << " " << bsystname << std::endl;      
      df_def=std::make_shared<ROOT::RDF::RNode>((*df_def)
						.Define("fatjet0_trkjet0_sf"+bsystname, [btag_syst_idx](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float
							{  return trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]; },
							{"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_SF_"+btagWP})
						.Define("fatjet0_trkjet1_sf"+bsystname, [btag_syst_idx](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float
							{  return trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx]; },
							{"fatjet_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_SF_"+btagWP})
						);
    }
}
