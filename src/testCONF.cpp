#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "Sample.h"
#include "CONFHbbAnalysis.h"

int main(int argc, char* argv[])
{
  // Parse arguments
  if(argc!=2)
    {
      std::cerr << "usage: " << argv[0] << " nthreads" << std::endl;
      return 1;
    }
  uint32_t nthreads=std::atoi(argv[1]);

  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../samples.yml");

  // Samples to save
  std::vector<std::string> saves={"Pythia8_dijet.mc16a","Sherpa_Zqq.mc16a","Sherpa_Wqq.mc16a","Sherpa_Vqq.mc16a","Pythia8_ttbar.mc16a","data1516"};
  
  // Histogram list
  std::vector<ROOT::RDF::TH1DModel> hists;
  hists.push_back({"Hcand_m","",48,0,240});
  hists.push_back({"Hcand_pt","",200,0,2000});
  hists.push_back({"averageInteractionsPerCrossing","",100,-0.5,99.5});

  std::map<std::string, std::shared_ptr<ROOT::RDF::RNode> CONFHbbAnalysis::*> selections;
  selections["sr"]=&CONFHbbAnalysis::df_sr;
  selections["vr"]=&CONFHbbAnalysis::df_vr;

  std::vector<std::string> cachelist={"w"}; // Cache branches for each selection
  std::transform(hists.begin(),hists.end(),std::back_inserter(cachelist),
		 [](const ROOT::RDF::TH1DModel& histDef) { return histDef.fName.Data(); });
  
  // Analysis
  ROOT::RDF::RResultPtr<TH1D> h;

  for(const std::string& save : saves)
    {
      std::cout << "Processing " << save << std::endl;
      TFile *fh_out=TFile::Open((save+".root").c_str(),"RECREATE");

      std::shared_ptr<ROOT::RDF::RNode> df=sf[save]->dataframe();
      uint32_t nevents=df->Count().GetValue();
      CONFHbbAnalysis a(df,sf[save]->isMC());

      for(const std::pair<std::string, std::shared_ptr<ROOT::RDF::RNode> CONFHbbAnalysis::*> selection : selections)
	{
	  fh_out->mkdir(selection.first.c_str())->cd();
	  ROOT::RDF::RNode cache=(a.*(selection.second))->Cache(cachelist);     
	  for(const ROOT::RDF::TH1DModel& hdef : hists)
	    {
	      h=cache.Histo1D(hdef,hdef.fName.Data(),"w");
	      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	      h->Write(hdef.fName);
	      std::chrono::steady_clock::time_point end   = std::chrono::steady_clock::now();
	      std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " Hz" << std::endl;
	}
	}
      fh_out->Close();
    }
}
