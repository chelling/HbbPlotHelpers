#include "TruthHbbAnalysis.h"

#include "Defines.h"

TruthHbbAnalysis::TruthHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC)
  : m_isMC(isMC)
{
  df_root=rootdf;

  // Definitions of weights
  if(isMC)
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root)
						   .Define("w", [](float weight)->float
							   { return weight * (36.2e3+41.0e3+58.5e3); },
							   {"weight_norm"})
						   );
    }
  else
    {
      df_weight=std::make_shared<ROOT::RDF::RNode>((*df_root)
						   .Define("w", []()->float { return 1.; })
						   );
    }  
  
  // Jet selection
  df_triggerjet=std::make_shared<ROOT::RDF::RNode>((*df_weight)
						   .Filter([](const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m) -> bool { return trigger_jet_ptmass(fatjet_pt,fatjet_m,450,60); }, {"fatjet_pt","fatjet_m"} , "Trigger Jet")
						   );

  df_dijet=std::make_shared<ROOT::RDF::RNode>((*df_triggerjet)
					      .Filter([](int32_t nfatjet, const std::vector<float>& fatjet_pt)->bool { return nfatjet>=2 && fatjet_pt[1]>200; }, {"nfatjet","fatjet_pt"})
					      .Define("fatjet0_m", [](const std::vector<float>& fatjet_m)->float { return fatjet_m[0]; }, {"fatjet_m"})
					      .Define("fatjet1_m", [](const std::vector<float>& fatjet_m)->float { return fatjet_m[1]; }, {"fatjet_m"})
					      );

  // Signal candidate selection
  df_Hcand =std::make_shared<ROOT::RDF::RNode>((*df_dijet).Define("Hcand_idx",
								  [](const std::vector<float>& fatjet_eta, const std::vector<float>& fatjet_phi, float Higgs_eta, float Higgs_phi)->uint32_t
								  {
								    for(uint32_t Hcand_idx=0; Hcand_idx<fatjet_eta.size(); Hcand_idx++)
								      {
									float dR2=pow(fatjet_eta[Hcand_idx]-Higgs_eta,2)+pow(TVector2::Phi_mpi_pi(fatjet_phi[Hcand_idx]-Higgs_phi),2);
									if(dR2<0.2) return Hcand_idx;
								      }
								    return 666;
								  },
								  {"fatjet_eta", "fatjet_phi", "Zboson_eta", "Zboson_phi"})
					       .Filter([](uint32_t Hcand_idx)->bool { return Hcand_idx<2;}, {"Hcand_idx"})
					       .Define("Hcand_pt", define_float_idx, {"fatjet_pt", "Hcand_idx"})
					       .Define("Hcand_m" , define_float_idx, {"fatjet_m" , "Hcand_idx"})
					       );
  
  df_srl_inc=std::make_shared<ROOT::RDF::RNode>((*df_Hcand)
						.Filter([](uint32_t Hcand_idx)->bool { return Hcand_idx==0; }, {"Hcand_idx"})
						);
  
  df_srs_inc=std::make_shared<ROOT::RDF::RNode>((*df_Hcand)
						.Filter([](uint32_t Hcand_idx)->bool { return Hcand_idx==1; }, {"Hcand_idx"})
						);
}
