#include "Sample.h"

#include <TFile.h>

uint32_t dskey(int runNumber, int mcChannelNumber)
{ return 1000*runNumber+mcChannelNumber; }

void Sample::setScale(double scale)
{ m_scale=scale; }

void Sample::setWeightIdx(uint32_t idx)
{ m_weightIdx=idx; m_weightIdxSet=true; }

void Sample::setExtraWeights(const std::vector<uint32_t>& extraWeights)
{ m_extraWeights=extraWeights; m_extraWeightsSet=true; }

void Sample::init_root()
{
  m_root=std::make_shared<ROOT::RDataFrame>("outTree",filelist());
}

std::shared_ptr<ROOT::RDataFrame> Sample::root()
{
  if(!m_root)
    init_root();

  return m_root;
}

std::shared_ptr<ROOT::RDF::RNode> Sample::dataframe()
{
  if(!m_root)
    init_root();

  if(isMC())
    {
      std::unordered_map<uint32_t, double               > theScales      =scales      ();
      std::unordered_map<uint32_t, uint32_t             > theWeightIdx   =weightIdx   ();
      std::unordered_map<uint32_t, std::vector<uint32_t>> theExtraWeights=extraWeights();

      return std::make_shared<ROOT::RDF::RNode>((*m_root)
						.Define("weightIdx",
							[theWeightIdx](int runNumber, int mcChannelNumber) -> uint32_t {
							  std::unordered_map<uint32_t,uint32_t>::const_iterator idx=theWeightIdx.find(dskey(runNumber,mcChannelNumber));
							  if(idx==theWeightIdx.end()) return 0;
							  else return idx->second;
							},
							{"runNumber","mcChannelNumber"})
						.Define("mcEventWeightDef",
							[](uint32_t weightIdx, const std::vector<float>& mcEventWeights) -> float { return mcEventWeights[weightIdx]; },
							{"weightIdx", "mcEventWeights"})
						.Define("extraWeight",
							[theExtraWeights](int runNumber, int mcChannelNumber, const std::vector<float>& mcEventWeights) -> float {
							  std::unordered_map<uint32_t, std::vector<uint32_t>>::const_iterator idxs=theExtraWeights.find(dskey(runNumber,mcChannelNumber));
							  if(idxs==theExtraWeights.end()) return 1.;
							  else
							    return std::accumulate(idxs->second.begin(),
										   idxs->second.end(),
										   1.,
										   [&mcEventWeights](float a, uint32_t idx)->float { return a*mcEventWeights.at(idx); }
										   ); 
							},
							{"runNumber", "mcChannelNumber", "mcEventWeights"})
						.Define("weight_norm",
							[theScales](int runNumber, int mcChannelNumber, float weight_xs, float mcEventWeightDef, float extraWeight) -> float { return weight_xs*theScales.at(dskey(runNumber,mcChannelNumber))*mcEventWeightDef*extraWeight; },
							{"runNumber","mcChannelNumber","weight_xs","mcEventWeightDef","extraWeight"})
						);
    }
  else
    {
      return std::make_shared<ROOT::RDF::RNode>((*m_root)
						.Define("weight_norm", []() -> float {return 1.;})
						);
    }
      
}



