#!/bin/bash

#
# Define all the things to run over
signals=(Higgs_ggf Higgs_VBF Higgs_VH Higgs_ttH Sherpa_Wqq Sherpa_Zqq Herwig_Wqq Herwig_Zqq Powheg_ttbar Powheg_singletop Sherpa_Wlnu)
backgrounds=(data Pythia8_dijet Sherpa_ttbar Sherpa225_Wqq Sherpa225_Zqq)

OPTS="-t 8"
OPTS_SIG="-e ../data/Higgs_EW_corrections.root ${OPTS}"
OPTS_BKG="${OPTS}"

OUTDIR=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/dibjetISR_boosted/data_2020221/cutflow

#
# Run over everything
for signal in ${signals[@]}
do
    echo ./src/cutflowEventWide ${OPTS_SIG} ${OUTDIR} ${sample}
done

for signal in ${backgrounds[@]}
do
    echo ./src/cutflowEventWide ${OPTS_BKG} ${OUTDIR} ${sample}
done
