import ROOT as ROOT
import argparse, os, sys, numpy
from array import array

def isGood(hist2D):
  xBins = hist2D.GetXaxis().GetNbins()
  yBins = hist2D.GetYaxis().GetNbins()
  for x in range(1,xBins):
    for y in range(1,yBins):
      content = hist2D.GetBinContent(x,y)
      if content < 16 and content != 0:
        return False
  return True

def BadBins(hist2D):
  arr = []
  xBins = hist2D.GetXaxis().GetNbins()
  yBins = hist2D.GetYaxis().GetNbins()
  for x in range(1,xBins):
    for y in range(1,yBins):
      content = hist2D.GetBinContent(x,y)
      if content < 16 and content != 0:
        arr.append([x,y])
  return arr

def GetBinEdges(hist2D):
  xEdges=[]
  yEdges=[]
  xBins = hist2D.GetXaxis().GetNbins()
  yBins = hist2D.GetYaxis().GetNbins()
  for x in range(1,xBins+2):
    xEdges.append(hist2D.GetXaxis().GetBinLowEdge(x))
  for y in range(1,yBins+2):
    yEdges.append(hist2D.GetYaxis().GetBinLowEdge(y))
  return xEdges, yEdges

def FindBadBin(hist2D):
  xBins = hist2D.GetXaxis().GetNbins()
  yBins = hist2D.GetYaxis().GetNbins()
  for x in range(1,xBins):
    for y in range(1,yBins):
      content = hist2D.GetBinContent(x,y)
      if content < 16 and content != 0:
        return x,y
  return False

def LookAround(hist2D, x, y):
  xBins = hist2D.GetXaxis().GetNbins()
  yBins = hist2D.GetYaxis().GetNbins()
  CandBins = {}
  for i in [x-1,x+1]:
    if i<1 or i>xBins:
      continue
    CandBins[i,y]=hist2D.GetBinContent(i,y)
  for j in [y-1,y+1]:
    if j<1 or j>yBins:
      continue
    CandBins[x,j]=hist2D.GetBinContent(x,j)
 
  val = hist2D.GetBinContent(x,y)
  OutBins = {}
  nPos = 0
  for key in CandBins:
    i=key[0]
    j=key[1]
    OutBins[i,j] = (CandBins[i,j]+val)/16 -1
    if OutBins[i,j] > 0:
      nPos+=1
  
  if nPos > 0:
    for key in OutBins:
      i = key[0]
      j = key[1]
      if OutBins[i,j]<=0.:
        OutBins[i,j] = 9999.
  
  if nPos > 0:
    return min(OutBins, key=OutBins.get)
  else:
    return max(OutBins, key=OutBins.get)

def MergeX(hist2D,x1,x2):
  if abs(x1-x2)!=1:
    print("INVALID")  
  nBinsX = hist2D.GetXaxis().GetNbins()
  nBinsY = hist2D.GetYaxis().GetNbins()
  inCont = numpy.zeros(shape=(nBinsX,nBinsY))
  for i in range(1,nBinsX+1):
    for j in range(1,nBinsY+1):
      inCont[i-1,j-1] = hist2D.GetBinContent(i,j)
  col_x1 = inCont[x1-1,:] 
  col_x2 = inCont[x2-1,:]
  col_sum = numpy.add(col_x1,col_x2)  
  xdel = min(x1,x2)
  outCont = numpy.delete(inCont,xdel-1,axis=0)
  outCont[xdel-1,:]=col_sum

  # create output histogram
  xEdges = GetBinEdges(hist2D)[0] 
  yEdges = GetBinEdges(hist2D)[1]
  xEdges = numpy.delete(xEdges, xdel)
  histName = hist2D.GetName()
  nBinsX = len(xEdges)-1
  nBinsY = len(yEdges)-1
  outPlot = ROOT.TH2F(histName, histName, nBinsX, array('f',xEdges), nBinsY, array('f',yEdges))
  for i in range(1,nBinsX+1): 
    for j in range(1,nBinsY+1):
      outPlot.SetBinContent(i,j,outCont[i-1,j-1])
  return outPlot  


def MergeY(hist2D,y1,y2):
  if abs(y1-y2)!=1:
    print("INVALID")
  nBinsX = hist2D.GetXaxis().GetNbins()
  nBinsY = hist2D.GetYaxis().GetNbins()
  inCont = numpy.zeros(shape=(nBinsX,nBinsY))
  for i in range(1,nBinsX+1):
    for j in range(1,nBinsY+1):
      inCont[i-1,j-1] = hist2D.GetBinContent(i,j)
  row_y1 = inCont[:,y1-1]
  row_y2 = inCont[:,y2-1]
  row_sum = numpy.add(row_y1,row_y2)
  ydel = min(y1,y2)
  outCont = numpy.delete(inCont,ydel-1,axis=1)
  outCont[:,ydel-1]=row_sum

  # create output histogram
  xEdges = GetBinEdges(hist2D)[0]
  yEdges = GetBinEdges(hist2D)[1]
  yEdges = numpy.delete(yEdges, ydel)
  histName = hist2D.GetName()
  nBinsX = len(xEdges)-1
  nBinsY = len(yEdges)-1
  outPlot = ROOT.TH2F(histName, histName, nBinsX, array('f',xEdges), nBinsY, array('f',yEdges))
  for i in range(1,nBinsX+1):
    for j in range(1,nBinsY+1):
      outPlot.SetBinContent(i,j,outCont[i-1,j-1])
  return outPlot

def RebinX(hist2D, newName, xEdges):
  hist2D     = hist2D.Clone("temp")
  xEdges_old = GetBinEdges(hist2D)[0]
  yEdges_old = GetBinEdges(hist2D)[1]
  yEdges     = yEdges_old
  nBinsX     = len(xEdges)-1
  nBinsY     = len(yEdges)-1
  outHist    = ROOT.TH2F(newName, newName, nBinsX, array('f',xEdges), nBinsY, array('f',yEdges))
  if(not all(x in xEdges_old for x in xEdges)):
    return False
  for y in range(1,nBinsY+1):
    for x in range(1,nBinsX+1):
      xEdges_sub = [xEdge for xEdge in xEdges_old if xEdge<xEdges[x] and xEdge>=xEdges[x-1]]
      val = 0.
      for xEdge_sub in xEdges_sub:
        xsub = hist2D.GetXaxis().FindBin(xEdge_sub)
        val += hist2D.GetBinContent(xsub,y)
      outHist.SetBinContent(x,y,val)
  return outHist

def RebinY(hist2D, newName, yEdges):
  hist2D     = hist2D.Clone("temp")
  xEdges_old = GetBinEdges(hist2D)[0]
  yEdges_old = GetBinEdges(hist2D)[1]
  xEdges     = xEdges_old
  nBinsX     = len(xEdges)-1
  nBinsY     = len(yEdges)-1
  outHist    = ROOT.TH2F(newName, newName, nBinsX, array('f',xEdges), nBinsY, array('f',yEdges))
  if(not all(y in yEdges_old for y in yEdges)):
    return False
  for x in range(1,nBinsX+1):
    for y in range(1,nBinsY+1):
      yEdges_sub = [yEdge for yEdge in yEdges_old if yEdge<yEdges[y] and yEdge>=yEdges[y-1]]
      val = 0.
      for yEdge_sub in yEdges_sub:
        ysub = hist2D.GetYaxis().FindBin(yEdge_sub)
        val += hist2D.GetBinContent(x,ysub)
      outHist.SetBinContent(x,y,val)
  return outHist

def main():
  # parse arguments
  parser = argparse.ArgumentParser(description='Creates reweight maps from pT/m correlation plots and saves them to file.')
  parser.add_argument('sysFileName', help="Path to syst file.")
  parser.add_argument('nomFileName', help="Path to nominal file.")
  parser.add_argument('outFileName', help="Path to output file.")
  args = parser.parse_args()

  # check if output files exist
  sysFile = ROOT.TFile(args.sysFileName,"READ")
  if sysFile.IsZombie():
    print("No file found with name "+sysFileName)
    sys.exit()
  nomFile = ROOT.TFile(args.nomFileName,"READ")
  if nomFile.IsZombie():
    print("No file found with name "+nomFileName)
    sys.exit()

  # create output file
  outFile = ROOT.TFile(args.outFileName,"RECREATE")

  # loop over regions
  regList = ["bbl","bbs","bxl","bxs","xxl","xxs"]
  #regList = ["crttbar"]

  xEdges = {}
  yEdges = {}
  
  # bbl
  xEdges["bbl"] = [60,90,120,140,150,160,165,170,175,180,185,190,195,200,210,220,240,260,280]
  yEdges["bbl"] = [250,460,470,480,490,500,510,520,530,540,550,560,580,600,650,700,800,1200]
  # bbs
  xEdges["bbs"] = [60,90,120,140,150,160,165,170,175,180,185,190,195,200,210,220,240,260,280] 
  yEdges["bbs"] = [250,300,320,340,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,510,520,530,540,550,560,580,600,650,700,800,1200]
  # bxl
  xEdges["bxl"] = [60,70,80,90,100,110,120,130,140,145,150,155,160,165,170,175,180,190,200,210,220,230,240,250,260,270,280]
  yEdges["bxl"] = [250,460,470,480,490,500,510,520,530,540,550,560,580,600,650,700,800,1200]
  # bxs
  xEdges["bxs"] = [60,70,80,90,100,110,120,130,140,145,150,155,160,165,170,175,180,190,200,210,220,230,240,250,260,270,280]
  yEdges["bxs"] = [250,300,320,340,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,510,520,530,540,550,560,580,600,650,700,800,1200]
  # xxl
  xEdges["xxl"] = [60,65,70,75,80,85,90,95,100,110,120,130,140,160,170,180,200,220,280]
  yEdges["xxl"] = [250,460,470,480,490,500,510,520,530,540,550,560,580,600,650,700,800,1200]
  # xxs
  xEdges["xxs"] = [60,65,70,75,80,85,90,95,100,110,120,130,140,160,170,180,200,220,280]
  yEdges["xxs"] = [250,300,320,340,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,510,520,530,540,550,560,580,600,650,700,800,1200]
  # crttbar
  xEdges["crttbar"] = [60,70,80,90,100,110,120,130,140,145,150,155,160,165,170,175,180,190,200,210,220,230,240,250,260,270,280]
  yEdges["crttbar"] = [250,300,320,340,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,510,520,530,540,550,560,580,600,650,700,800,1200] 

  for reg in regList:
    # import correlation plots
    sysHist = sysFile.Get(reg+"/Corr_m_pt").Clone()
    nomHist = nomFile.Get(reg+"/Corr_m_pt").Clone()

    # rebin correlation plots to custom binning
    sysHist = RebinX(sysHist, "tmp"         , xEdges[reg])
    sysHist = RebinY(sysHist, "sysHist_"+reg, yEdges[reg])

    nomHist = RebinX(nomHist, "tmp"         , xEdges[reg])
    nomHist = RebinY(nomHist, "nomHist_"+reg, yEdges[reg])

    # check if some bins have low statistics after rebinning 
    for hist in [sysHist, nomHist]:
      if isGood(hist):
        continue
      else:
        print("WARNING: "+hist.GetName()+": low statistics in bins "+str(BadBins(hist)))

    # divide correlation plots to create reweight map
    sysHist.Divide(nomHist) 
    sysHist.SetName("hMap_"+reg)
 
    # save maps to file 
    sysHist.Write("hMap_"+reg)
  
  sysFile.Close()
  nomFile.Close()
  outFile.Close()

main()
