from prot import triggertools
from prot import filetools
from prot import canvastools
from prot import plottools
from prot import histtools
from prot import utiltools

import ROOT
import array

def suggest_rebin(hist,min):
    bins=[hist.GetBinLowEdge(i) for i in range(1, hist.GetNbinsX()+2)]
    cont=[hist.GetBinContent(i) for i in range(1, hist.GetNbinsX()+1)]
    binidx=len(cont)-1
    while binidx>1:
        if cont[binidx]<min:
            cont[binidx-1]+=cont[binidx]
            del cont[binidx]
            del bins[binidx]
        binidx-=1
    return bins
    
def main():
    c=canvastools.canvas()
    c.SetGrid(True)
    
    filetools.filemap('trigger_Pythia8_dijet.mc16d.root','qcd17' )
    filetools.filemap('trigger_data17.root'             ,'data17')

    filetools.filemap('trigger_Pythia8_dijet.mc16e.root','qcd18' )
    filetools.filemap('trigger_data18.root'             ,'data18')



    #
    #
    triggertools.trigeff('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt_num','data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt_den',
                         fitrange=(370,500),
                         xtitle='Leading Jet p_{T} [GeV]',xrange=(350,500),
                         ytitle='Efficiency',yrange=(0.5,1),
                         text='data17, m_{J} > 100 GeV\nHLT_j390_a10t_lcw_jes_30smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                         lumi=0.176,sim=False)
    canvastools.save('HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fit_fatjet0_pt.png')

    varbin=suggest_rebin(utiltools.Get('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt_num'),min=1000)
    num=utiltools.Get('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt_num').Rebin(len(varbin), '', array.array('d',varbin))
    den=utiltools.Get('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt_den').Rebin(len(varbin), '', array.array('d',varbin))

    data=triggertools.trigeff(num,den).CreateGraph()
    qcd =triggertools.trigeff('qcd17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt_num' ,'qcd17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt_den' ).CreateGraph()

    plottools.graphs([(qcd,{'title':'Pythia 8 QCD','color':ROOT.kRed}),(data,{'title':'Data'})],
                     xtitle='Leading Jet p_{T} [GeV]',xrange=(0,1000),
                     ytitle='Efficiency',yrange=(0.95,1.01),
                     text='data17, m_{J} > 100 GeV\nHLT_j390_a10t_lcw_jes_30smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                     lumi=0.176,sim=False,
                     legend=(0.6,0.5))

    canvastools.save('HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_pt.png')

    #
    #
    triggertools.trigeff('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m_num','data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m_den',
                         fitrange=(20,100),
                         xtitle='Leading Jet Mass [GeV]',xrange=(0,200),
                         ytitle='Efficiency',yrange=(0.5,1),
                         text='data17, p_{T} > 500 GeV\nHLT_j390_a10t_lcw_jes_30smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                         lumi=0.176,sim=False)
    canvastools.save('HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fit_fatjet0_m.png')

    varbin=suggest_rebin(utiltools.Get('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m_num'),min=1000)
    num=utiltools.Get('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m_num').Rebin(len(varbin), '', array.array('d',varbin))
    den=utiltools.Get('data17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m_den').Rebin(len(varbin), '', array.array('d',varbin))

    data=triggertools.trigeff(num,den).CreateGraph()
    qcd =triggertools.trigeff('qcd17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m_num' ,'qcd17:/HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m_den' ).CreateGraph()

    plottools.graphs([(qcd,{'title':'Pythia 8 QCD','color':ROOT.kRed}),(data,{'title':'Data'})],
                     xtitle='Leading Jet Mass [GeV]',xrange=(0,200),
                     ytitle='Efficiency',yrange=(0.95,1.01),
                     text='data17, p_{T} > 500 GeV\nHLT_j390_a10t_lcw_jes_30smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                     lumi=0.176,sim=False,
                     legend=(0.6,0.5))

    canvastools.save('HLT_j390_a10t_lcw_jes_30smcINF_L1J100/fatjet0_m.png')

    #
    #
    triggertools.trigeff('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt_num','data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt_den',
                         fitrange=(400,500),
                         xtitle='Leading Jet p_{T} [GeV]',xrange=(350,500),
                         ytitle='Efficiency',yrange=(0.5,1),
                         text='data18, m_{J} > 100 GeV\nHLT_j420_a10t_lcw_jes_35smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                         lumi=0.017,sim=False)
    canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fit_fatjet0_pt.png')

    varbin=suggest_rebin(utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt_num'),min=500)
    num=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt_num').Rebin(len(varbin), '', array.array('d',varbin))
    den=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt_den').Rebin(len(varbin), '', array.array('d',varbin))

    data=triggertools.trigeff(num,den).CreateGraph()
    qcd =triggertools.trigeff('qcd18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt_num' ,'qcd18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt_den' ).CreateGraph()

    plottools.graphs([(qcd,{'title':'Pythia 8 QCD','color':ROOT.kRed}),(data,{'title':'Data'})],
                     xtitle='Leading Jet p_{T} [GeV]',xrange=(0,1000),
                     ytitle='Efficiency',yrange=(0.95,1.01),
                     text='data18, m_{J} > 100 GeV\nHLT_j420_a10t_lcw_jes_35smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                     lumi=0.017,sim=False,
                     legend=(0.6,0.5))

    #canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_pt.png')

    #
    #
    triggertools.trigeff('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m_num','data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m_den',
                         fitrange=(30,100),
                         xtitle='Leading Jet Mass [GeV]',xrange=(0,200),
                         ytitle='Efficiency',yrange=(0.5,1),
                         text='data18, p_{T} > 500 GeV\nHLT_j420_a10t_lcw_jes_35smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                         lumi=0.017,sim=False)
    canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fit_fatjet0_m.png')

    varbin=suggest_rebin(utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m_num'),min=500)
    num=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m_num').Rebin(len(varbin), '', array.array('d',varbin))
    den=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m_den').Rebin(len(varbin), '', array.array('d',varbin))

    data=triggertools.trigeff(num,den).CreateGraph()
    qcd =triggertools.trigeff('qcd18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m_num' ,'qcd18:/HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m_den' ).CreateGraph()

    plottools.graphs([(qcd,{'title':'Pythia 8 QCD','color':ROOT.kRed}),(data,{'title':'Data'})],
                     xtitle='Leading Jet Mass [GeV]',xrange=(0,200),
                     ytitle='Efficiency',yrange=(0.95,1.01),
                     text='data18, p_{T} > 500 GeV\nHLT_j420_a10t_lcw_jes_35smcINF_L1J100\nref HLT_j260_a10t_lcw_jes_L1J75',
                     lumi=0.017,sim=False,
                     legend=(0.6,0.5))

    canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF_L1J100/fatjet0_m.png')


    
    
    triggertools.trigeff('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt_num','data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt_den',
                         fitrange=(400,500),
                         xtitle='Leading Jet p_{T} [GeV]',xrange=(350,500),
                         ytitle='Efficiency',yrange=(0.5,1),
                         text='data18, m_{J} > 100 GeV\nHLT_j420_a10t_lcw_jes_35smcINF\nref HLT_j260_a10t_lcw_jes_L1J75',
                         lumi=0.0,sim=False)
    canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF/fit_fatjet0_pt.png')

    varbin=suggest_rebin(utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt_num'),min=500)
    num=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt_num').Rebin(len(varbin), '', array.array('d',varbin))
    den=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt_den').Rebin(len(varbin), '', array.array('d',varbin))

    data=triggertools.trigeff(num,den).CreateGraph()
    qcd =triggertools.trigeff('qcd18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt_num' ,'qcd18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt_den' ).CreateGraph()

    plottools.graphs([(qcd,{'title':'Pythia 8 QCD','color':ROOT.kRed}),(data,{'title':'Data'})],
                     xtitle='Leading Jet p_{T} [GeV]',xrange=(0,1000),
                     ytitle='Efficiency',yrange=(0.95,1.01),
                     text='data18, m_{J} > 100 GeV\nHLT_j420_a10t_lcw_jes_35smcINF\nref HLT_j260_a10t_lcw_jes_L1J75',
                     lumi=0.0,sim=False,
                     legend=(0.6,0.5))

    canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_pt.png')

    #
    #
    triggertools.trigeff('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m_num','data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m_den',
                         fitrange=(40,100),
                         xtitle='Leading Jet Mass [GeV]',xrange=(0,200),
                         ytitle='Efficiency',yrange=(0.98,1),
                         text='data18, p_{T} > 500 GeV\nHLT_j420_a10t_lcw_jes_35smcINF\nref HLT_j260_a10t_lcw_jes_L1J75',
                         lumi=0.0,sim=False)
    canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF/fit_fatjet0_m.png')

    varbin=suggest_rebin(utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m_num'),min=500)
    num=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m_num').Rebin(len(varbin), '', array.array('d',varbin))
    den=utiltools.Get('data18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m_den').Rebin(len(varbin), '', array.array('d',varbin))

    data=triggertools.trigeff(num,den).CreateGraph()
    qcd =triggertools.trigeff('qcd18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m_num' ,'qcd18:/HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m_den' ).CreateGraph()

    plottools.graphs([(qcd,{'title':'Pythia 8 QCD','color':ROOT.kRed}),(data,{'title':'Data'})],
                     xtitle='Leading Jet Mass [GeV]',xrange=(0,200),
                     ytitle='Efficiency',yrange=(0.95,1.01),
                     text='data18, p_{T} > 500 GeV\nHLT_j420_a10t_lcw_jes_35smcINF\nref HLT_j260_a10t_lcw_jes_L1J75',
                     lumi=0.0,sim=False,
                     legend=(0.6,0.5))

    canvastools.save('HLT_j420_a10t_lcw_jes_35smcINF/fatjet0_m.png')

main()
